from src.clean_data import clean_data
from src.f_score import f_score
from src.g_score import g_score
from src.m_score import m_score
from src.other_ratios import calculate_ratios
import pandas as pd
import numpy as np
import secrets

#splits list into randomly chosen two parts where the first part has length = fraction*len(list)
def split_list(o_list, fraction):
    size = fraction*len(o_list)
    rightlist = o_list.copy()
    leftlist = []
    while len(leftlist) < size :
        choice = secrets.choice(rightlist)
        leftlist.append(choice)
        rightlist.remove(choice)
    return leftlist,rightlist


if __name__ == '__main__' :
    
    
    for i in range(1992,2015, 4) :
        co_list = []
        picked_instr = []
        not_picked = []
        j = 0
        for date in range(i, min(i+4, 2015)) :
            co_list.append(pd.read_csv(f'./dta/{str(date)}_company_data.csv', header = [0,1]))
            y_list = co_list[j][str(i+j)]
            picked_instr = picked_instr + y_list[y_list["Picked"]==1]["Instrument"].to_list()
            not_picked = not_picked + y_list[y_list["Picked"]==0]["Instrument"].to_list()
            j+= 1
        print(picked_instr)
        picked_test, picked_train = split_list(picked_instr, 0.2)
        npicked_test, npicked_train = split_list(not_picked, 0.2)
        
        j = 0;
        for df in co_list :
            year_n = str(i + j)
            if i + j == 1993 :
                for k in range(5):
                    diff = list(set(df[str(i+j+k)].columns)-set(co_list[0][str(i+k)].columns))
                    for item in diff :
                        df.drop((str(i+j+k), item), axis = 1, errors = 'ignore', inplace = True)
            test = df[pd.DataFrame(df[year_n]["Instrument"].to_list()).isin(picked_test).any(1)]
            test = test.append(df[pd.DataFrame(df[year_n]["Instrument"].to_list()).isin(npicked_test).any(1)])
            test.to_csv(f'./dta/test/{year_n}_company_data_test.csv', index = False)

            train = df[pd.DataFrame(df[year_n]["Instrument"].to_list()).isin(picked_train).any(1)]
            train = train.append(df[pd.DataFrame(df[year_n]["Instrument"].to_list()).isin(npicked_train).any(1)])
            train.to_csv(f'./dta/train/{year_n}_company_data_train.csv', index = False)

            j+=1

