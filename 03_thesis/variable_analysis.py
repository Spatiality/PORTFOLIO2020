import pandas as pd
import numpy as np
from src.models import load_dataframes, xgb
from src.stats import correlation_matrix, descr_stats, variable_importance, get_col_names, scale
from sklearn.model_selection import StratifiedKFold


namelist = [["Total Assets, Reported"],["Total Current Assets"],["Accounts Receivable - Trade, Net"],
    ["Accounts Receivable - Trade, Gross","Accounts Receivable (CF)"],["Short Term Investments"],
    ["Non-Current Marketable Securities, Supplemental"],
    ["Property, Plant And Equipment, Total - Gross","Property/Plant/Equipment, Total - Net"],
    ["Cash","Cash and Short Term Investments"],["Total Liabilities"],["Total Current Liabilities"],
    ["Long Term Debt","Total Long Term Debt"],["Accounts Payable"],["Total Debt"],["Total Equity"],
    ["Revenue","Total Revenue"],["Net Income Incl Extra Before Distributions"],["Gross Profit"],
    ["EBIT"],["EBITDA"],["Advertising Expense"],["Research And Development"],
    ["Selling/General/Administrative Expense, Total"],["Depreciation And Amortization"],
    ["Interest Expense","Total Interest Expenses"],["Operating Expenses"],["Cost of Revenue, Total"],
    ["Total Inventory"],["Principal Payments from Securities"],["Dividends Payable"],
    ["Notes Payable/Short Term Debt"],["Capex, Discrete"],["Net Income Before Extraordinary Items"],
    ["Basic EPS Including Extraordinary Items"],["Operating Income"],["Common Stock, Total"],
    ["Market Value for Company"],["Retained Earnings (Accumulated Deficit)"],
    ["ROE"],["current_ratio"],["acidity"],["TIE"],["invtry turnover"],
    ["fixed_asset_turnover"],["LEVER"],["Gross Margin"],
    ["ROA","$flo","CFO","ACCRUAL","DLIQUID","DTURN","DROA","FSCORE"],
    ["ROA","$flo","CFO","DLEVER","ISSUES","DMARGIN","SGR","RDINT","CAPINT","ADINT","VARSGR","VARROA","GSCORE"],
    ["DSRI","GMI","AQI","SGI","DEPI","SGAI","TATA","LGVI","MSCORE"],
    ["X1","X2","X3","X4","X5","ZSCORE"],
    ["WCTA","Size","TLTA","CLCA","NITA","FUTL","CHIN","OENEG","INTWO","O-score"],
    ["DGMAR", "DGPOA", "DROE", "D_ROA", "D$FLO", "QMJ_PROFIT", "QMJ_GROWTH", "QMJ_SAFETY", "QMJ"]]

indnames, names = get_col_names()

namelist = namelist + [indnames]

varlist = (["Baseline","Scaled","Total Assets, Reported","Total Current Assets",
    "Accounts Receivable - Trade, Net",
    "Accounts Receivable","Short Term Investments",
    "Non-Current Marketable Securities",
    "Property, Plant And Equipment",
    "Cash","Total Liabilities","Total Current Liabilities",
    "Long Term Debt","Accounts Payable","Total Debt","Total Equity",
    "Revenue","Net Income Incl Extra Before Distributions","Gross Profit",
    "EBIT","EBITDA","Advertising Expense","Research And Development",
    "Selling/General/Administrative Expense, Total","Depreciation And Amortization",
    "Interest Expenses","Operating Expenses","Cost of Revenue, Total",
    "Total Inventory","Principal Payments from Securities","Dividends Payable",
    "Notes Payable/Short Term Debt","Capex, Discrete","Net Income Before Extraordinary Items",
    "Basic EPS Including Extraordinary Items","Operating Income","Common Stock, Total",
    "Market Value for Company","Retained Earnings (Accumulated Deficit)",
    "ROE","current_ratio","acidity","TIE","invtry turnover",
    "fixed_asset_turnover","LEVER","Gross Margin",
    "FSCORE","GSCORE","MSCORE","ZSCORE","O-score","QMJ", "Industries"])


def scaleset() :
    df = pd.read_csv(f'./dta/train/{1992}_company_data_train.csv', header = [0,1])
    df = scale(df, 1992)
    arr1 = df.iloc[:,2:].to_numpy()
    for date in range(1993,2015):
        dfn = pd.read_csv(f'./dta/train/{date}_company_data_train.csv', header = [0,1])
        dfn = scale(dfn, date)
        newarray = dfn.iloc[:,2:].to_numpy()
        arr1 = np.append(arr1, newarray, axis = 0)
    #if not is_test :
    #    np.random.shuffle(arr1)

    return arr1[:,1:], arr1[:,0]



if __name__ == '__main__' :
    
    #load files into one single np array that disregards the numbering by year
    
    X_train_baseline, y_train_baseline = load_dataframes(is_test = False)
    X_train_scaled, y_train_scaled = scaleset()

    #define class weights
    #weights = {0:1.00, 1:1000.0}
    unique, counts = np.unique(y_train_baseline, return_counts=True)
    weights = dict(zip(unique, counts))
    X_train = []
    y_train = []

    
    for var in namelist:
        x_t, y_t = load_dataframes(is_test = False, dropname = var)
        X_train.append(x_t)
        y_train.append(y_t)
    
        
    scores = np.zeros(len(namelist) + 2)
    skf = StratifiedKFold(n_splits = 10)
    m = xgb(weights)
    fold = 0
    for tr_i, te_i in skf.split(X_train_baseline, y_train_baseline) :

        fold += 1
        m.fit(X_train_baseline[tr_i], y_train_baseline[tr_i])
        scores[0] += m.evaluate(y_train_baseline[te_i], m.predict(X_train_baseline[te_i]))

        m.fit(X_train_scaled[tr_i], y_train_scaled[tr_i])
        scores[1] += m.evaluate(y_train_scaled[te_i], m.predict(X_train_scaled[te_i]))

        for i in range(len(namelist)):
            
            print(f'now trying performance with dropping {varlist[i+1]}')
            m.fit(X_train[i][tr_i], y_train[i][tr_i])
            scores[i+2] += m.evaluate(y_train[i][te_i], m.predict(X_train[i][te_i]))
    
    scoreframe = pd.Series(scores/fold, index = varlist)
    scoreframe.sort_values(inplace = True)

    print(scoreframe)
    scoreframe.to_csv('variable_scores_by_obscuration.csv')


