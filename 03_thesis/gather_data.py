from src.get_co_list import get_co_list
from src.drop_reinsert_picks import drop_reinsert_picks
from src.assign_year import assign_year
from src.pull_data import pull_company_data
from src.add_business_are import add_business_area


if __name__ == '__main__' :

    #screen for companies that can possibly be analysed
    get_co_list()

    #drop the picks made by warren buffett from this list
    drop_picks()
    
    #assign years to be considered in to the non-picked companies
    assign_year()

    #fill in the company fundamentals
    pull_data()

    #add the area of business
    add_business_area()

    
