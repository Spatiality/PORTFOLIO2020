import pandas as pd



#calculates the f-score and inserts it at the end fo each row
def calculate_ratios(df, starting_year) :
    #insert correct names for fixd assets & cmn stock
    fixed_assets = "Property, Plant And Equipment, Total - Gross"
    common_stock = "Common Stock, Total"
    for i in range(starting_year, starting_year + 5) :
        year = str(i)
        df_curr = df[year]
        #calculate ROE = net_income[t] / (total_assets[t-1] - total_liabilities[t-1]
        df[year,"ROE"] = (df_curr["Net Income Incl Extra Before Distributions"]
            /(df_curr["Total Assets, Reported"] - df_curr["Total Current Liabilities"]))
        """
        try:
            df.drop((year,"debt_to_assets"), inplace = True)
        except:
            pass
        """
        #calculate current_ratio = current_assets/current_liabilities
        df[year, "current_ratio"] = df_curr["Total Current Assets"]/df_curr["Total Current Liabilities"]
        #calculate acid_test = (current_assets - inventory)/current_liabilities
        df[year, "acidity"] = ((df_curr["Total Current Assets"] - df_curr["Total Inventory"])
                                / df_curr["Total Current Liabilities"])
        #calculate times_interest_earned = EBIT / interest_expense
        df[year,"TIE"] = df_curr["EBIT"]/ df_curr["Interest Expense"]
        #calculate ~ inventory turnover = cost_of_revenue / inventory
        df[year, "invtry turnover"] = df_curr["Cost of Revenue, Total"]/ df_curr["Total Inventory"]
        #calculate fixed_asset_turnover = revenue / fixed_assets
        df[year, "fixed_asset_turnover"] = df_curr["Total Revenue"] / df_curr[fixed_assets]
        df[year, "Gross Margin"] = (df_curr["Total Revenue"] - df_curr["Cost of Revenue, Total"])/df_curr["Total Revenue"]
        df[year, "LEVER"] = df_curr["Total Debt"] / df_curr["Total Assets, Reported"]
        if i == starting_year + 4 :
            df_last = df[str(i-1)]
            df_bfstart = df[str(starting_year + 1)]
            df_start = df[str(starting_year)]
            df[year, "DGPOA"] = ((df_curr["Total Revenue"] - df_last["Cost of Revenue, Total"]
                - df_bfstart["Total Revenue"] + df_start["Cost of Revenue, Total"])/
                df_bfstart["Total Assets, Reported"])
            df[year, "DROE"] = ((df_curr["Net Income Incl Extra Before Distributions"]
                - df_start["Net Income Incl Extra Before Distributions"])
                / (df_start["Total Assets, Reported"] - df_start["Total Current Liabilities"]))
            df[year, "D_ROA"] = ((df_curr["Net Income Incl Extra Before Distributions"]
                - df_start["Net Income Incl Extra Before Distributions"]) / df_start["Total Assets, Reported"])
            df[year, "D$FLO"] = ((df_curr["$flo"] - df_bfstart["$flo"])/ df_bfstart["Total Assets, Reported"])
            df[year, "DGMAR"] = ((df_curr["Total Revenue"] - df_curr["Cost of Revenue, Total"]
                - df_start["Total Revenue"] + df_start["Cost of Revenue, Total"])
                / df_start["Total Revenue"])
    df.fillna(0.00001618, inplace = True)
    return df




                


        
