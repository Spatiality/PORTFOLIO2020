import pandas as pd
import csv

import eikon as ek


def screenexpr(yearn, nextyearn) :
    return 'SCREEN(U(IN(Equity(active,public,primary))),IN(TR.ExchangeCountryCode,"US"),TR.EBIT(Period=FY'+yearn+')>=-9999999999999,TR.EBIT(Period=FY'+nextyearn+')>=-9999999999999,CURN=USD)'


ek.set_app_key(open("app_key.txt", "r").read())


def get_co_list() :

    for i in range(1997,2019) :
        yearn = str(i-5)
        endyear = str(i-1)
        co_list =ek.get_data(instruments = [screenexpr(yearn,endyear)],
         fields = ["TR.CommonName"], debug = False) 

        print(co_list)

        co_list[0].to_csv('./dta/' + yearn+'_companies.csv')


