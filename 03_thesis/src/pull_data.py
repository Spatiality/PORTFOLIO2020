import pandas as pd
import csv
from time import sleep
import eikon as ek

def pull_company_data() :

    data_items = ['TR.TotalAssetsReported', 'TR.TotalCurrentAssets', 'TR.AcctsReceivTradeNet', 'TR.AcctsReceivTradeGross', 'TR.AccountsReceivable', 'TR.ShortTermInvestments', 'TR.LTMarketableSecuritiesSuppl', 'TR.PptyPlantEqpmtTtlGross', 'TR.PropertyPlantEquipmentTotalNet', 'TR.Cash','TR.CashAndSTInvestments','TR.TotalLiabilities', 'TR.TotalCurrLiabilities', 'TR.LTDebt', 'TR.TotalLongTermDebt', 'TR.AccountsPayable', 'TR.TotalDebtOutstanding', 'TR.TotalEquity', 'TR.Revenue', 'TR.TotalRevenue', 'TR.NetIncome','TR.GrossProfit','TR.EBIT','TR.EBITDA', 'TR.AdvertisingExpense', 'TR.ResearchAndDevelopment','TR.SgaExpenseTotal', 'TR.DepreciationAmort', 'TR.InterestExpense', 'TR.TotalInterestExpense', 'TR.OperatingExpenses', 'TR.CostofRevenueTotal', 'TR.TotalInventory', 'TR.PrincipalPaymentsFromSecurities', 'TR.DividendsPayable', 'TR.NotesPayableSTDebt', 'TR.CapexCFStmt']

    #'TR.BusinessSector'

    ek.set_app_key(open("app_key.txt", "r").read())


    for date in range(1992,2015) :

        co_list = pd.read_csv(f'./dta/{str(date)}_to_consider.csv')

        co_names = list(co_list['Instrument'].values)

        co_list.columns = :w
        pd.MultiIndex.from_product([[str(date)],list(co_list.columns.values)],names = ['year','item'])


        for i in range(date,date + 5) :
            year = str(i)

            co_data, err = ek.get_data(co_names, data_items, parameters = {"SDate": 'FY'+year})

            d_names = list(co_data.columns.values)

            d_names1 = pd.MultiIndex.from_product([[year],d_names], names = ['year','item'])

            co_data.columns = d_names1

            co_list = pd.concat([co_list,co_data.iloc[:,1:]],axis = 1)

        co_list.to_csv(f'./dta/{str(date)}_company_data.csv', index = False)

