import pandas as pd



#calculates the f-score and inserts it at the end fo each row
def f_score(df, starting_year) :
    #insert correct names for fixd assets & cmn stock
    fixed_assets = "Property, Plant And Equipment, Total - Gross"
    common_stock = "Common Stock, Total"
    for i in range(starting_year + 1, starting_year + 5) :
        year = str(i)
        df_last = df[str(i-1)]
        df_curr = df[year]
        #calculate ROA = net_income[t] / total_assets[t-1]
        df[year,"ROA"] = df_curr["Net Income Incl Extra Before Distributions"]/df_last["Total Assets, Reported"]
        #calculate cash_flow = EBIT[t] + depreciation[t] - fixed_assets[t] + fixed_assets[t-1]
        #                      - total_current_assets[t] + total_current_liabilities[t]
        #                      + total_current_assets[t-1] - total_current_liabilities[t-1]
        df[year, "$flo"] = (df_curr["EBIT"] + df_curr["Depreciation And Amortization"]
            - df_curr[fixed_assets] + df_last[fixed_assets]
            - df_curr["Total Current Assets"] + df_curr["Total Current Liabilities"]
            + df_last["Total Current Assets"] - df_last["Total Current Liabilities"] )
        #calculate CFO = cash_flow[t] / total_assets[t-1]
        df[year, "CFO"] = df[year, "$flo"] /df_last["Total Assets, Reported"]
        #calculate ACCRUAL = (net_income[t] - cash_flow[t]) / total_assets[t-1]
        df[year, "ACCRUAL"] = ((df_curr["Net Income Incl Extra Before Distributions"] - df[year, "$flo"])
            / df_last["Total Assets, Reported"])
        #calculate DLIQUID = current_assets[t] / current_liabilities[t]
        #                    - current_assets[t-1] / current_liabilities[t-1]
        df[year, "DLIQUID"] = (df_curr["Total Current Assets"]/df_curr["Total Current Liabilities"]
            - df_last["Total Current Assets"]/df_last["Total Current Liabilities"])
        #calculate DLEVER = (lt_debt[t] - lt_debt[t-1])
        #                   /0.5*(total_assets[t] + total_assets[t-1])
        df[year, "DLEVER"] = ((df_curr["Total Long Term Debt"] - df_last["Total Long Term Debt"])
            /0.5*(df_curr["Total Current Assets"] + df_last["Total Revenue"]))
        #calculate ISSUES = common_stock[t] - common_stock[t-1]
        df[year, "ISSUES"] = df_curr[common_stock] - df_last[common_stock]
        #calculate DMARGIN = ((sales - COGS)/sales)[t]
        #                    - ((sales - COGS)/sales)[t-1]
        df[year, "DMARGIN"] = ((df_curr["Total Revenue"] - df_curr["Cost of Revenue, Total"]) /df_curr["Total Revenue"]
            -  (df_last["Total Revenue"] - df_last["Cost of Revenue, Total"])/df_last["Total Revenue"])
        if i >= starting_year + 2 :
            df.fillna(0.00001618, inplace = True)
            df_bflast = df[str(i-2)]
            #calculate DTURN = (sales[t]/total_assets[t-1]) - (sales[t-1]/total_assets[t-2])
            df[year,"DTURN"] = (df_curr["Total Revenue"] / df_last["Total Revenue"]
                - df_last["Total Revenue"] / df_bflast["Total Revenue"])
            #calculate DROA = ROA[t] - ROA[t-1]
            df[year,"DROA"] = df[year,"ROA"] - df[str(i-1), "ROA"]
            #calculate FSCORE
            df[year,"FSCORE"] = 0
            for j in range(df.shape[0]) :
                if df[year,"ROA"].iloc[j] > 0 :
                    df[year,"FSCORE"].iloc[j] += 1
                if df[year,"CFO"].iloc[j] > 0 :
                    df[year,"FSCORE"].iloc[j] += 1
                if df[year,"DROA"].iloc[j] > 0 :
                    df[year,"FSCORE"].iloc[j] += 1
                if df[year,"ACCRUAL"].iloc[j] < 0 :
                    df[year,"FSCORE"].iloc[j] += 1
                if df[year,"DLEVER"].iloc[j] < 0 :
                    df[year,"FSCORE"].iloc[j] += 1
                if df[year,"ISSUES"].iloc[j] <= 0 :
                    df[year,"FSCORE"].iloc[j] += 1
                if df[year,"DLIQUID"].iloc[j] > 0 :
                    df[year,"FSCORE"].iloc[j] += 1
                if df[year,"DMARGIN"].iloc[j] > 0 :
                    df[year,"FSCORE"].iloc[j] += 1
                if df[year,"DTURN"].iloc[j] > 0 :
                    df[year,"FSCORE"].iloc[j] += 1
    

    return df




                


        
