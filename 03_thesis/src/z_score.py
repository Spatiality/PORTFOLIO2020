import pandas as pd



#calculates the z-score and inserts it at the end of each row
#has to be calculated AFTER g-score
def z_score(df, starting_year, med_X4 = 0.0) :
    #insert correct name for operating income
    operating_income = "Operating Income"
    for i in range(starting_year, starting_year + 5) :
        year = str(i)
        df_curr = df[year]

        #calculate  X1
        df[year, "X1"] = (df_curr["Total Current Assets"]-df_curr["Total Current Liabilities"])/df_curr["Total Assets, Reported"]
        #calculate X2
        df[year, "X2"] = (df_curr["Retained Earnings (Accumulated Deficit)"])/df_curr["Total Assets, Reported"]
        #calculate X3
        df[year, "X3"] = df_curr["EBIT"]/df_curr["Total Assets, Reported"]
        #calculate X4
        if i < 2000 :
            df[year, "X4"] = med_X4
        else :
            df[year, "X4"] = df_curr["Market Value for Company"]/df_curr["Total Liabilities"]
        #calculate X5
        df[year, "X5"] = df_curr["Total Revenue"]/df_curr["Total Assets, Reported"]

        #calculate Z-Score
        df[year, "ZSCORE"] = (df[year,"X1"]*1.2 + df[year,"X2"]*1.4 + df[year,"X3"]*3.107
            + df[year,"X4"]*3.3 + df[year,"X5"])

    return df




                


        
