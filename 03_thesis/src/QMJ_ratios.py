import pandas as pd
import numpy as np
from src.stats import get_mean

def z_(series) :
    ranks = series.rank()
    return (ranks - ranks.mean())/ranks.std()

def calculate_measure(df, starting_year, varlist) :

    z_arrs = z_(get_mean(df, varlist[0], starting_year)[0])
    for var in varlist[1:] :
        meanvar, stdvar = get_mean(df, var, starting_year)
        z_arrs += z_(meanvar)
    return z_arrs


#calculates the o-score and inserts it at the end of each row
#has to be calculated AFTER z-score

def QMJ(df, starting_year) :
    endyear = str(starting_year + 4)

    #Profitability ranking
    varlist = ["Gross Margin", "ROA", "ROE", "CFO", "ACCRUAL"]
    df[endyear, "QMJ_PROFIT"] = z_(calculate_measure(df, starting_year, varlist))

    #Growth ranking
    varlist = ["DGPOA", "DROE", "D_ROA", "D$FLO", "DGMAR"]
    df[endyear, "QMJ_GROWTH"] = z_(calculate_measure(df, starting_year, varlist))

    #Safety ranking
    varlist = ["O-score", "ZSCORE"]
    lever_mean, std = get_mean(df, "LEVER", starting_year)
    roe_mean, roe_std = get_mean(df, "ROE", starting_year, stddev = True)
    df[endyear, "QMJ_SAFETY"] = z_(
          calculate_measure(df, starting_year, varlist)
        + z_(lever_mean*(-1.0))
        + z_(roe_std*(-1.0)))

    #Calculate super-ranking
    df[endyear, "QMJ"] = z_(
          z_(df[endyear, "QMJ_PROFIT"])
        + z_(df[endyear, "QMJ_GROWTH"])
        + z_(df[endyear, "QMJ_SAFETY"]))

    
    return df


                


        
