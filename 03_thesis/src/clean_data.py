import pandas as pd
import numpy as np

#Compares source to target entry, returns target value if it's a number, else it returns source value
def compare_entries(source, target) :
    if target == np.nan:
        return source
    return target
    
def compare_and_subtract(source, target, diff) :
    if target == np.nan :
        if diff != np.nan :
            return source - diff
        return source
    return target

def csti_c_sti(csti, cash, sti) :
    if csti == np.nan :
        if sti == np.nan :
            return cash
        elif cash == np.nan :
            return sti
        return cash + sti
    return csti


#general clean-up, consolidation, & infilling of data
def clean_data(df, starting_year) :
    #enter name for fixed assets
    fixed_assets = "Property, Plant And Equipment, Total - Gross"
    for i in range(starting_year, starting_year + 5) :
        year = str(i)
        df_curr = df[year]
        #consolidate Revenue vs. Total Revenue, the latter being the preferred column
        df[year, "Total Revenue"] = df_curr.apply(lambda x : compare_entries(x["Revenue"], x["Total Revenue"]), axis = 1)
        #drop Revenue
        df[year].drop("Revenue", axis=1, inplace=True)
        #consolidate Total LT debt vs. LT debt
        df[year, "Total Long Term Debt"] = df_curr.apply(lambda x : compare_entries(x["Long Term Debt"], x["Total Long Term Debt"]), axis = 1)
        #drop LT debt
        df[year].drop("Long Term Debt", axis=1, inplace=True)
        #consolidate Total Liabilities vs. Total Current Liabilities, the latter being used but less complete
        df[year, "Total Current Liabilities"] = df_curr.apply(lambda x : compare_and_subtract(x["Total Liabilities"], x["Total Current Liabilities"], x["Total Long Term Debt"]), axis = 1)
        #consolidate gross PPE vs net PPE, the latter being more complete
        df[year,"Property, Plant And Equipment, Total - Gross"] = df_curr.apply(lambda x :
            compare_entries(x["Property/Plant/Equipment, Total - Net"], x["Property, Plant And Equipment, Total - Gross"]), axis = 1)
        #consolidate Total Assets vs Total Current Assets, the latter being used more often but less complete by using fixed assets
        df[year, "Total Current Assets"] = df[year].apply(lambda x : compare_and_subtract(x["Total Assets, Reported"], x["Total Current Assets"], x[fixed_assets]), axis = 1)
        #consolidate cash, cash + sti, sti
        df[year, "Cash and Short Term Investments"] = df[year].apply(lambda x : csti_c_sti(x["Cash and Short Term Investments"], x["Cash"], x["Short Term Investments"]), axis = 1)
        #drop cash, sti
        df[year].drop("Cash", axis=1, inplace=True)
        df[year].drop("Short Term Investments", axis=1, inplace=True)
        #drop Total Interest Expenses
        df[year].drop("Total Interest Expenses", axis=1, inplace=True)
    #drop all rows which miss many entries == more than 110 NaN entries
    selection = df.isnull().sum(axis=1) > 110
    df.drop(df[selection].index, axis = 0, inplace = True)
    #fill all other missing entries with 0.00001618
    df.fillna(0.00001618, inplace = True)
    return df




                


        
