import pandas as pd
import csv



#strips the 'traded in' ending in the RIC's and returns everything before the '.'
def strip(strings) :
    substring = ""
    checked = False
    for i in strings :
        if i == '.' :
            checked = True
        if not checked :
            substring = substring + i
    return substring

#checks if 'substring' of length n matches the first n characters of 'string'
def partial_match(string, substring):
    if len(string) < len(substring) :
        return False
  
    if substring != string[:len(substring)] :
        return False
    else :
        return True


# takes out all BKa entries in the general selection of companies
def drop_picks() :

    picks = pd.read_csv("./dta/BKa_first_ownership_dates.csv")

    to_delete = list(picks.iloc[:,0])

    #since we want to check the partial matches by hand, we don't want to check them multiple times
    #hence we make a dictionary with companies we already checked in context with the suspect picked companies
    safe = {}

    for i in to_delete :
        safe[i] = []
    safe['berk'] = []

    for i in range(1992,2015) :
        yearn = str(i)
        co_list = pd.read_csv('./dta/' + yearn+'_companies.csv')
        co_list.drop(co_list[co_list.Instrument.isin(to_delete)].index, inplace= True)
        for j in range(len(picks.iloc[:,0])):
            sym = picks.iloc[j,0]
            name = picks.iloc[j,1] 
            for k in range(0,len(co_list.iloc[:,0])) :
                    cosym = co_list.iloc[k,1]
                    coname = co_list.iloc[k,2] 
                    if partial_match(coname, name) or partial_match(name,coname) or ((strip(cosym) == strip(sym)) and (cosym not in safe[sym])) or (('erkshire' in coname) and (cosym not in safe['berk'])) :
                        print('possible match found!')
                        print('instrument & company on list: ' + cosym + '; ' + co_list.iloc[k,2])
                        print('       versus found in picks: ' + sym   + '; ' + name)
                        print('delete y/n?')
                        dec = input()
                        if dec == 'y' :
                            to_delete.append(cosym)
                        elif dec == 'n' and ('erkshire' not in coname):
                            safe[sym].append(cosym)
                        elif dec == 'n' :
                            safe['berk'].append(cosym)
        co_list.drop(co_list[co_list.Instrument.isin(to_delete)].index, inplace= True)

        co_list.to_csv('./dta/' + yearn+'_companies_cleaned.csv')
