import pandas as pd
import random
import secrets

def assign_year() :

    not_chosen = {}
    chosen = {}


    for year in range(1992,2015):
        not_chosen[year] = {}
        chosen[year] = []

    picks = pd.read_csv("./dta/BKa_first_ownership_dates.csv")


    for year in range(1992,2015):
        maxyear = min(2014, year + 4)
        df_list = list()
        for i in range(year,maxyear + 1) :
            df_list.append(pd.read_csv("./dta/"+str(i)+"_companies_cleaned.csv"))    
        for i in range(len(df_list[0].index)) :
            sym = df_list[0].iloc[i,2]
            name = df_list[0].iloc[i,3]
            if (sym not in not_chosen[year]) and [sym,name, '0'] not in chosen[year] :
                choosable = list()
                choosable.append(year)
                for j in range(year + 1, maxyear + 1) :
                    if i in df_list[j - year].iloc[:,2] :
                        choosable.append(i)
                choice = secrets.choice(choosable)
                for j in range(year, min(choice + 4,2015)) :
                    if choice == j :
                        chosen[j].append([sym,name, '0'])
                    else :
                        not_chosen[j][sym] = True

        column_names = ["Instrument","Company_Name", "Picked"]
        for i in range(len(picks.index)):
            if picks.iloc[i,2] == year + 5 :
                sym = picks.iloc[i,0]
                name = picks.iloc[i,1] 
                chosen[year].append([sym,name,'1'])
        df = pd.DataFrame(chosen[year],columns= column_names).to_csv("./dta/" + str(year) + "_to_consider.csv", index = False)
        print("in the year " + str(year) + ", " + str(year) + "!")

                





