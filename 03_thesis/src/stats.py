import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from sklearn.decomposition import PCA
import seaborn as sns
from src.models import XGB, svm
from sklearn import preprocessing
from mpl_toolkits.mplot3d import Axes3D

scalevars = ["Total Current Assets",
    "Accounts Receivable - Trade, Net","Accounts Receivable - Trade, Gross","Accounts Receivable (CF)",
    "Short Term Investments",
    "Non-Current Marketable Securities, Supplemental","Property, Plant And Equipment, Total - Gross",
    "Property/Plant/Equipment, Total - Net",
    "Cash","Cash and Short Term Investments","Total Liabilities","Total Current Liabilities",
    "Long Term Debt","Total Long Term Debt","Accounts Payable","Total Debt","Total Equity",
    "Revenue","Total Revenue","Net Income Incl Extra Before Distributions","Gross Profit",
    "EBIT","EBITDA","Advertising Expense","Research And Development",
    "Selling/General/Administrative Expense, Total","Depreciation And Amortization",
    "Interest Expense","Total Interest Expenses","Operating Expenses","Cost of Revenue, Total",
    "Total Inventory","Principal Payments from Securities","Dividends Payable",
    "Notes Payable/Short Term Debt","Capex, Discrete","Net Income Before Extraordinary Items",
    "Basic EPS Including Extraordinary Items","Operating Income",
    "Market Value for Company","Retained Earnings (Accumulated Deficit)"]

def scale(df, yearn) :
    for year in range(yearn, yearn + 5):
        for var in scalevars :
            df[str(year), var] = df[str(year), var]/df[str(year),"Total Assets, Reported"]
    df.replace([np.inf, -np.inf, "NaN", "inf", "-inf"], np.nan, inplace = True)
    df.fillna(0.00001618, inplace = True)
    return df


def get_col_names() :
    df = pd.read_csv(f'./dta/{1992}_company_data.csv', header = [0,1])

    #get names of industry columns
    
    dfn = df.iloc[:,3:]
    
    names = list(dfn['1996'].columns)
    nnames = list(dfn['1992'].columns)
    indnames = []
    for name in nnames :
        if name not in names and name not in ['Instrument','Company_Name'] :
            indnames.append(name)

    return indnames, names

def get_mean(df, var, yearn, stddev = False) :
    varframe = df[(str(yearn+4), var)]
    for year in range(yearn, yearn + 4) :
        try :
            varframe = pd.concat([varframe,df[(str(year), var)]], axis = 1, sort = False)
        except:
            continue

    try :
        mean = varframe.mean(axis = 1)
    except :
        mean = varframe
    std = 0.0
    if stddev :
        
        try :
            std = varframe.std(axis = 1)
        except :
            pass
    return mean, std
            
    

def descr_stats(stddev = False) :
    indnames, names = get_col_names()
    picked = pd.DataFrame(columns = ['Instrument','Company_Name'] + names)
    not_picked = pd.DataFrame(columns = ['Instrument','Company_Name'] + names)
    cnt = 0
    idf = ["picked", "npicked"]
    for ndf in [picked, not_picked] :
        for yearn in range(1992,2015) :
            df = pd.read_csv(f'./dta/{yearn}_company_data.csv', header = [0,1])
            picked_df = df[df[str(yearn)]['Picked'] == 1]
            not_picked_df = df[df[str(yearn)]['Picked'] == 0]
            dta_arr = [picked_df, not_picked_df]

            temp = pd.DataFrame(columns = ['Instrument','Company_Name'])
            for mon in ['Instrument','Company_Name'] :
                temp[mon] = dta_arr[cnt][(str(yearn), mon)]
            for var in names :
                mean, std = get_mean(dta_arr[cnt], var, yearn, stddev)
                temp[var] = mean
                if stddev:
                    temp[var+'_std'] = std
            ndf = ndf.append(temp, ignore_index = True, sort = False)
            del temp                   
            print(f'BEEP! Arrived in year {yearn}')
        ndf.to_csv(f'./dta/stats_{idf[cnt]}.csv', index = False)
        cnt+=1


def variable_importance() :
    picked = pd.read_csv('./dta/stats_picked.csv')
    not_picked = pd.read_csv('./dta/stats_npicked.csv')

    train_true_length = int(picked.shape[0] *0.9)
    test_true_length = picked.shape[0] - train_true_length

    train_false_length = int(not_picked.shape[0]*0.9)
    test_false_length = not_picked.shape[0] - train_false_length
     
 
    X_train = picked.iloc[:train_true_length,2:].to_numpy()
    X_train = np.append(X_train, not_picked.iloc[:train_false_length,2:].to_numpy(), axis = 0)

    y_train = np.concatenate([np.ones(train_true_length), np.zeros(train_false_length)])

    X_test = picked.iloc[train_true_length:,2:].to_numpy()
    X_test = np.append(X_test, not_picked.iloc[train_false_length:,2:].to_numpy(), axis = 0)

    y_test = np.concatenate([np.ones(test_true_length), np.zeros(test_false_length)])

    unique, counts = np.unique(y_train, return_counts=True)
    weights = dict(zip(unique, 1.0/counts))
            
    m = XGB(weights)
    m.fit(X_train, y_train)
    #importances = pd.Series(m._model.feature_importances_, index = list(picked.columns)[2:] )
    
    #neat little solution to map the variable names to the xboost naming found on stackoverflow
    mapper = {'f{0}'.format(i): v for i, v in enumerate(list(picked.columns)[2:])}
    importances ={mapper[k]: v for k, v in m._model.get_score(importance_type = 'gain').items()}

    for var in list(picked.columns)[2:] :
        if var not in importances :
            importances[var] = 0
    importances =  pd.Series(importances)
    
    y_pred = m.predict(X_test)
    m.evaluate(y_test,y_pred)
        
    importances = importances.sort_values()
    print(importances)
    importances.to_csv('feature_importances.csv')

def principal_components(three_d = False) :

    picked = pd.read_csv('./dta/stats_picked.csv').iloc[:,2:]
    not_picked = pd.read_csv('./dta/stats_npicked.csv').iloc[:,2:]
    true_len = picked.shape[0]

    data = picked.append(not_picked)
    
    data.fillna(0.00001618, inplace = True)

    scaler = preprocessing.StandardScaler()
    scaler.fit(data)
    data = scaler.transform(data)

    pca = PCA(n_components=3)
    transformed = pca.fit_transform(data)

    if three_d :
        fig = plt.figure()
        ax = fig.add_subplot(111, projection='3d')

        ax.scatter(transformed[true_len:,0],transformed[true_len:,1],transformed[true_len:,2], '^', color='red', label='not_picked')

        ax.scatter(transformed[:true_len,0],transformed[:true_len,1],transformed[:true_len,2], 'o', color='blue', label='picked')
        #plt.xlim([-3.3e14-1.8639925301e26,-1.85e14-1.8639925301e26])
        #plt.ylim([-20e10-6.7488603324e24,-6.5e10-6.7488603324e24])
        ax.set_xlabel('PC_1')
        ax.set_ylabel('PC_2')
        ax.set_zlabel('PC_3')

    else:
    
        plt.plot(transformed[true_len:,0],transformed[true_len:,1], '^', markersize=2, color='red', alpha=0.5, label='not_picked')

        plt.plot(transformed[0:true_len,0],transformed[0:true_len,1], 'o', markersize=2, color='blue', alpha=0.5, label='picked')
        #plt.xlim([-3.3e14-1.8639925301e26,-1.7e14-1.8639925301e26])
        #plt.ylim([-20e11-6.7488603324e24,-16e11-6.7488603324e24])
        plt.tick_params(labelbottom=False, labelleft = False)   
        plt.xlabel('PC_1')
        plt.ylabel('PC_2')
        plt.legend()
        plt.title('Transformed samples with class labels')
    
    #plt.tight_layout()
    plt.show()

def class_averages() :
    picked = pd.read_csv('./dta/stats_picked.csv').iloc[:,2:]
    not_picked = pd.read_csv('./dta/stats_npicked.csv').iloc[:,2:]
    true_len = picked.shape[0]
    names = pd.read_csv('./dta/stats_picked.csv')

    data = picked.append(not_picked)
    
    data.fillna(0.00001618, inplace = True)

    scaler = preprocessing.StandardScaler()
    scaler.fit(data)
    data = scaler.transform(data)

    picked = data[:true_len,:]
    not_picked = data[true_len:,:]
 
    mean_picked = picked.mean(axis = 0)
    mean_npicked = not_picked.mean(axis = 0)

    diff_mean = (mean_picked - mean_npicked)
    diff_mean = pd.Series(diff_mean, index = names.columns.to_list()[2:]) 
    diff_mean = diff_mean.reindex(diff_mean.abs().sort_values(ascending = False).index)


    print('The scaled difference between the averages is:')
    print(diff_mean)

    diff_mean.to_csv('./stats/mean_differences.csv')
    
   
    
def correlation_matrix() :
    picked = pd.read_csv(f'./dta/stats_picked.csv')
    not_picked = pd.read_csv('./dta/stats_npicked.csv')

    corr = picked.iloc[:,2:].corr() - not_picked.iloc[:,2:].corr()
    
    ax = sns.heatmap(corr, vmin=-1, vmax=1, center=0, square=True, xticklabels = 1,yticklabels = 1)

    ax.set_xticklabels(ax.get_xticklabels(), fontsize = 2.2)
    ax.set_yticklabels(ax.get_yticklabels(), fontsize = 2.2)
    plt.tight_layout()
    plt.savefig('corr_diff.png', dpi = 400)
    #exclude them from the matrix
    



