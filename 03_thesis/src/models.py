import pandas as pd
import numpy as np
from sklearn import preprocessing
from sklearn.model_selection import StratifiedKFold, cross_val_score
from sklearn.svm import SVC
from sklearn.linear_model import LinearRegression
from sklearn.metrics import balanced_accuracy_score, confusion_matrix, roc_auc_score, roc_curve, precision_score, recall_score
from xgboost import XGBClassifier
import xgboost as xgb
from keras.models import Sequential
from keras.callbacks import ModelCheckpoint
from keras.layers import Dense, Dropout, BatchNormalization, Lambda
import tensorflow as tf
import matplotlib.pyplot as plt
import keras
from sklearn.decomposition import PCA
from mpl_toolkits.mplot3d import Axes3D

from keras.backend.tensorflow_backend import set_session
from keras.backend.tensorflow_backend import clear_session
from keras.backend.tensorflow_backend import get_session

# Reset Keras Session to prevent OOM allocation
# Copied from https://forums.fast.ai/t/how-could-i-release-gpu-memory-of-keras/2023/17
def reset_keras(classifier):
    sess = get_session()
    clear_session()
    sess.close()
    sess = get_session()

    try:
        del classifier 
    except:
        pass

    #print(gc.collect()) # if it's done something you should see a number being outputted

    # use the same config as you used to create the session
    config = tf.ConfigProto()
    config.gpu_options.per_process_gpu_memory_fraction = 1
    config.gpu_options.visible_device_list = "0"
    set_session(tf.Session(config=config))



def NNet(feature_num, neurons = 600, act = 'sigmoid', d_rate = 0.0, layers = 4, fft = False, initial_bias = None):
    # Create model
    if initial_bias is not None:
        initial_bias = tf.keras.initializers.Constant(initial_bias)
    ann = Sequential()
    ann.add(Dense(int(neurons), input_dim=feature_num, activation=act))
    ann.add(Dropout(d_rate))
    ann.add(BatchNormalization())

    for i in range(1,layers+1) :
        ann.add(Dense(int(neurons/1.2**i), input_dim=feature_num, activation=act))  # leaky
        ann.add(Dropout(d_rate))
        ann.add(BatchNormalization())
        if i == 2 and fft :
            ann.add(Lambda(lambda v: tf.cast(tf.spectral.fft(tf.cast(v,dtype=tf.complex64)),tf.float32)))
    if initial_bias is not None:
        ann.add(Dense(1, bias_initializer = initial_bias, activation = 'sigmoid'))
    else :
        ann.add(Dense(1, activation = 'sigmoid'))
    # Compile model
    ann.compile(loss = 'binary_crossentropy', optimizer='nadam',
        metrics=['accuracy',keras.metrics.Recall(name='recall'),
            keras.metrics.AUC(name='auc', curve = 'PR'), keras.metrics.Precision(name='precision')])
    return ann



class model :

    scaler = preprocessing.StandardScaler()
    _threshold = 0.5    
    
    def __init__(self, weights):
        self._model = LinearRegression(normalize = False)
        self._name = 'Linear Regression'
        self._weights = weights
        

    def fit(self,X_train, y_train) :
        self.scaler.fit(X_train)
        X_train = self.scaler.transform(X_train)
        self._model.fit(X_train,y_train)

    def predict(self,X_test, proba = True) :
        X_test = self.scaler.transform(X_test)
        y_pred = self._model.predict(X_test)
        if proba == False :
            y_pred = [int(i > self._threshold) for i in y_pred]
        return y_pred

    def evaluate(self,y_test,y_pred) :

        score = roc_auc_score(y_test, y_pred)
        
        print(f"The area under the curve for {self._name} is:")
        print(score)
        print(f"The balanced accuracy for {self._name} is:")
        print(self.accuracy(y_test,y_pred))
        print(f"The precision for {self._name} is:")
        print(self.precision(y_test,y_pred))
        print(f"The recall for {self._name} is:")
        print(self.recall(y_test,y_pred))

        
        return score

    def accuracy(self, y_test, y_pred, show = False) :

        y_pred = [int(i > self._threshold) for i in y_pred]

        score = balanced_accuracy_score(y_test, y_pred)
        if show == True :
            print(f"The balanced accuracy score for {self._name} is:")
            print(score)
        return score

    def precision(self, y_test, y_pred) :
        y_pred = [int(i > self._threshold) for i in y_pred]
        return precision_score(y_test, y_pred)


    def recall(self, y_test, y_pred) :
        y_pred = [int(i > self._threshold) for i in y_pred]
        return recall_score(y_test, y_pred)


    def roc(self, y_test,y_pred) :
        
        return roc_curve(y_test, y_pred, pos_label=1.0)

    def plot_roc(self, y_test, y_pred) :
        
        fpr, tpr, thresh = self.roc(y_test, y_pred)

        plt.plot(fpr, tpr, label=f"{self._name}: AUC {self.evaluate(y_test,y_pred):.2f}, Accuracy {self.accuracy(y_test,y_pred):.2f}")

    def show(self) :
        plt.legend(loc = 4)
        plt.show()

    def clear_plot(self) :
        plt.clf()

    def show_PCA(self, y_test, y_pred, three_d = False, dim1 = 0, dim2 = 1) :

        picked = pd.read_csv('./dta/stats_picked.csv')
        not_picked = pd.read_csv('./dta/stats_npicked.csv')
        true_len = picked.shape[0]
        false_len = not_picked.shape[0]

        data = picked.iloc[:,2:].append(not_picked.iloc[:,2:])
    
        data.fillna(0.00001618, inplace = True)

        scaler = preprocessing.StandardScaler()
        scaler.fit(data)
        data = scaler.transform(data)

        pca = PCA(n_components=3)
        transformed = pca.fit_transform(data) 

        base = 0
        names_selected = []
        names_nselected = []

        for date in range(1992,2015):
            dfn = pd.read_csv(f'./dta/test/{date}_company_data_test.csv', header = [0,1])
            sz = dfn.shape[0]
            cnt = 0
            for pred in y_pred[base: base + sz] :
                if pred > self._threshold :
                    names_selected.append(dfn[str(date)]['Company_Name'].iloc[cnt])
                else :
                    names_nselected.append(dfn[str(date)]['Company_Name'].iloc[cnt])
                cnt+=1
            base+= sz

        

        true_positives = picked['Company_Name'].isin(names_selected).to_numpy()
        false_negatives = picked['Company_Name'].isin(names_nselected).to_numpy()
        true_positives = np.append(true_positives, np.zeros(false_len, dtype = bool))
        false_negatives = np.append(false_negatives, np.zeros(false_len, dtype = bool))
        false_positives = not_picked['Company_Name'].isin(names_selected).to_numpy()
        true_negatives = not_picked['Company_Name'].isin(names_nselected).to_numpy()
        false_positives = np.append(np.zeros(true_len, dtype = bool), false_positives)
        true_negatives = np.append(np.zeros(true_len, dtype = bool), true_negatives)

        print(false_negatives.shape)
        print(transformed[false_negatives,2])
        
        if three_d :
            fig = plt.figure()
            ax = fig.add_subplot(111, projection='3d')

            ax.scatter(transformed[true_negatives,0],transformed[true_negatives,1],transformed[true_negatives,2], '^', color='red', label='true_negative')

            ax.scatter(transformed[false_positives,0],transformed[false_positives,1],transformed[false_positives,2], '^', color='orange', label='false_positive')

            ax.scatter(transformed[false_negatives,0],transformed[false_negatives,1],transformed[false_negatives,2], 'o', color='purple', label='false_negative')

            ax.scatter(transformed[true_positives,0],transformed[true_positives,1],transformed[true_positives,2], 'o', color='blue', label='true_positive')

            #plt.xlim([-3.3e14-1.8639925301e26,-1.85e14-1.8639925301e26])
            #plt.ylim([-20e10-6.7488603324e24,-6.5e10-6.7488603324e24])
            ax.set_xlabel('PC_1')
            ax.set_ylabel('PC_2')
            ax.set_zlabel('PC_3')

        else:
            plt.plot(transformed[true_negatives,dim1],transformed[true_negatives,dim2], '^', markersize=2, color='red', alpha=0.5, label='true_negative')
            
            plt.plot(transformed[false_negatives,dim1],transformed[false_negatives,dim2], '^', markersize=2, color='orange', alpha=0.5, label='false_negative')

            plt.plot(transformed[false_positives,dim1],transformed[false_positives,dim2], 'o', markersize=2, color='purple', alpha=0.5, label='false_positive')

            plt.plot(transformed[true_positives,dim1],transformed[true_positives,dim2], 'o', markersize=2, color='blue', alpha=0.5, label='true_positive')
            #plt.xlim([-3.3e14-1.8639925301e26,-1.7e14-1.8639925301e26])
            #plt.ylim([-20e11-6.7488603324e24,-16e11-6.7488603324e24])
            plt.tick_params(labelbottom=False, labelleft = False)   
            plt.xlabel(f'PC_{dim1+1}')
            plt.ylabel(f'PC_{dim2+1}')
            plt.legend()
            plt.title('Transformed samples with class labels')                


    def display_selected(self,y_pred) :
        base = 0
        names = []
        for date in range(1992,2015):
            dfn = pd.read_csv(f'./dta/test/{date}_company_data_test.csv', header = [0,1])
            sz = dfn.shape[0]
            cnt = 0
            for pred in y_pred[base:base + sz] :
                if pred > self._threshold :
                    names.append([pred, dfn[str(date)]['Company_Name'].iloc[cnt], dfn[str(date)]['Picked'].iloc[cnt], date+4])
                cnt+= 1
            base += sz

        names.sort()

        n_df = pd.DataFrame(names, columns = ['Prediction','Company Name', 'Picked', 'Year picked'])
        
        for n in names :
            print(n)
        return n_df

    def optimal_threshold(self, y_true, y_pred) :
        fpr, tpr, thresh = self.roc(y_true, y_pred)
        max_acc = 0.0
        best_t = self._threshold
        for t in thresh :
            self._threshold = t
            acc_temp = self.accuracy(y_true, y_pred)
            if acc_temp > max_acc :
                max_acc = acc_temp
                best_t = t
        self._threshold = best_t
                  
        
class svm(model) :
    
    _threshold = 0.0

    #overrides the model definition
    def __init__(self,weights) :
        self._weights = weights
        self._model = SVC(class_weight = 'balanced')
        self._name = 'SVM'

    def fit(self,X_train, y_train, CV = False) :

        self.scaler.fit(X_train)
        X_train = self.scaler.transform(X_train)

        skf = StratifiedKFold(n_splits = 10)
        #BEST feature transformation is sigmoid
        kernels = ["sigmoid"]

        #evaluate optimal weights, C, etc

        #optimal C : 0.1 [0.01, 0.1,1.0, 10.0, 100.0, 1000.0, 10000.0]
        #optimal weights: balanced [10, 50, 100, 500, 1000, 'balanced']

        #optimal C     0.02 [0.02, 0.04, 0.1, 0.15, 0.30, 1.0]
        #optimal gamma 0.01 [0.001, 0.01, 0.1, 1.0, 10.0, 100.0]

        #optimal C     0.01 [0.01, 0.015, 0.02, 0.025, 0.03]
        #optimal gamma 0.01 [0.002, 0.0025, 0.005, 0.01, 0.02, 0.05]

        #optimal C     0.01 [0.00001, 0.0001, 0.001, 0.01]
        #optimal gamma 0.014[0.006, 0.008, 0.01, 0.014]

        #optimal C      0.08    [0.01, 0.015, 0.03, 0.045, 0.06, 0.08, 0.1]
        #optimal gamma "scale"  [0.012, 0.014, 0.016, "scale"]

        #optimal coef0 -0.1 [-1.0, -0.1, -0.001, 0, 0.001, 0.01, 1.0]
        #optimal C_s  .07  [.07, 0.08, 0.09]

        if CV :
            C_s = [.07, 0.08, 0.09]
            weights = [-1.0, -0.1, -0.001, 0, 0.001, 0.01, 1.0]

            C_sz = len(C_s)
            w_sz = len(weights)
            
            scores = np.zeros((C_sz,w_sz))
            fold = 0
            for tr_i, te_i in skf.split(X_train, y_train) :
                fold += 1
                for i in range(C_sz) :
                    for j in range(w_sz) :

                        self._model = SVC(class_weight = 'balanced', kernel = "sigmoid", C = C_s[i], gamma = 'scale', coef0 = weights[j])
                        print(f"Evaluating SVM for C of {C_s[i]} and a coef0 of {weights[j]} in fold {fold}.")
                        self._model.fit(X_train[tr_i], y_train[tr_i])
                        scores[i,j] += self.evaluate(y_train[te_i], self._model.predict(X_train[te_i]))
            print("the score matrix for the CV is:")
            print(scores)
            a = np.unravel_index(scores.argmax(), scores.shape)
                
            self._model = SVC(class_weight = 'balanced', kernel = "sigmoid", C = C_s[a[0]],
                gamma = 'scale', coef0 = weights[a[1]])
        else:
            self._model = SVC(class_weight = 'balanced',
                kernel = "sigmoid", C = 0.07, gamma = 'scale', coef0 = -.1)

        self._model.fit(X_train,y_train)

    def predict(self,X_test, proba = True) :
        X_test = self.scaler.transform(X_test)

        if proba == False :
            return self._model.predict(X_test)
    
        return self._model.decision_function(X_test)


class XGB(model) :

    def __init__(self, weights) :

        self._weights = weights
        self._model = xgb.Booster({'nthread': 4}) 
        self._name = 'XGBoost'

    def fit(self,X_train, y_train, CV = False, remember = True) :

        self.scaler.fit(X_train)
        X_train = self.scaler.transform(X_train)

        dtrain = xgb.DMatrix(X_train, label = y_train)

        #cross-validated max_depth, learning_rate, gamma, reg_alpha, reg_lambda, weak learner model
        #BEST Model is gblinear
        #BEST no_estimators [50, 100, 200, 400, 800]
        #BEST LR is around 0.002
        #BEST LAMBDA is around 20.0
        #BEST ALPHA is around 0.004

        #explore parameters for boosted trees

    
        #BEST LR    0.001   [0.001,0.01, 0.1, 1.0]
        #BEST max_depth 3   [3,5,7,9,10]

        #BEST min_split_loss 0.0 [0.0, 0.01, 0.1, 1.0, 10.0]
        #BEST subsample      0.5 [0.5,0.65,0.8,0.95,1.0]


        #BEST reg_lambda  20.0  [0.001, 0.01,0.1, 1.0, 10.0] [20.0,50.0,100.0,150.0] 
        #BEST min_child_weight 0.00 [0.00, 0.01, 0.1, 1.0, 10.0]

        #BEST reg_alpha     [0.001, 0.01,0.1, 1.0] (has no influence?)

        #BEST boosting rounds 25 [10,15,20,25]
        #BEST max depth 12 [3,4,5,7,10,12]
    
        #BEST boosting rounds 200 [25, 50, 100, 200]
        #BEST max depth        12  [12, 15, 20, 30, 100]

        #BEST boosting rounds 1000 [200, 350, 600, 1000]
        #BEST max depth       12   [11, 12, 13, 14, 200]

        #BEST boosting rounds  [800, 1000, 2000, 4000] dpt 4 : 4000
        #BEST max depth        [4, 11, 12] rnds 4000 : 12 



        if CV :
            depths = [4, 12]
            dep_sz = len(depths)
            rates = [4000]
            rt_sz = len(rates)

            skf = StratifiedKFold(n_splits = 10)
            scores = np.zeros((dep_sz,rt_sz))
            fold = 0
            for tr_i, te_i in skf.split(X_train, y_train) :
                fold += 1
                for i in range(dep_sz) :
                    for j in range(rt_sz) :
                        params = {'booster' : 'gbtree', 'max_depth': depths[i],
                            'scale_pos_weight' : self._weights[1]/self._weights[0],
                            'eta': 0.001, 'objective': 'binary:logistic', 'lambda' : 20.0}
                        dtrain_cv = xgb.DMatrix(X_train[tr_i], label = y_train[tr_i])
                        self._model = xgb.train(params, dtrain_cv, num_boost_round=rates[j])
                        dtest_cv = xgb.DMatrix(X_train[te_i])
                        print(f"Evaluating XGB for a max_depth of {depths[i]} and {rates[j]} rounds in fold {fold}.")
                        scores[i,j] += self.evaluate(y_train[te_i], self._model.predict(dtest_cv))
            print("the score matrix for the CV is:")
            print(scores)
            a = np.unravel_index(scores.argmax(), scores.shape)
            params = {'booster' : 'gbtree', 'max_depth': depths[a[0]],
                    'scale_pos_weight' : self._weights[1]/self._weights[0],
                    'eta': 0.001, 'objective': 'binary:logistic',
                    'lambda' : 20.0, 'eval_metric' : ['auc', 'rmse', 'aucpr']}
            self._model = xgb.train(params, dtrain, num_boost_round=rates[a[1]])
        else:

            params = {'booster' : 'gbtree', 'max_depth': 12,
                'scale_pos_weight' : self._weights[1]/self._weights[0],
                'eta': 0.001, 'objective': 'binary:logistic', 'lambda' : 20.0}
            if remember == False :

                self._model = xgb.train(params, dtrain, num_boost_round=4000)
                self._model.save_model('./temp/xgb_model.json')

            else :

                self._model.load_model('./temp/xgb_model.json')


    def predict(self,X_test, proba = True) :
        X_test = self.scaler.transform(X_test)
        dtest = xgb.DMatrix(X_test)
        y_pred = self._model.predict(dtest)
        if proba == False :
            y_pred = [int(i > self._threshold) for i in y_pred]
        return y_pred



class NN(model) :
    def __init__(self,weights) :
        self._weights = weights
        self._name = 'NN'
        self._model = NNet(200)

    def fit(self,X_train, y_train, CV = False, remember = True) :
        
        self.scaler.fit(X_train)
        X_train = self.scaler.transform(X_train)

        #TODO: Try out sample weights determined by holding_period*amount_held

        #TODO: cross-validate the number of neurons, noise levels, layers, activation functions, and weights

        #BEST neurons  100.0    [100.0, 200.0, 300.0, 400.0]
        #BEST weights 1000.0    [100.0, 333.0, 1000.0, 3333.0, 10000.0]

        #BEST activation "sigmoid" ["relu", "sigmoid", "tanh"]
        #BEST neurons   900  [100.0,  300.0, 600.0, 900.0]

        #BEST noise lvls 0.5 [0.0, 0.1, 0.35, 0.5] 
        #BEST neurons    100 / 900 [100.0, 450, 900]

        #BEST layers     6    [4,6,7]
        #BEST neurons  150    [100.0, 150, 900.0]

        #BEST weights  450.0     [200.0, 450.0, 1000.0, 2000.0]
        #BEST fft      False     [True, False]

        #BEST model .8856 acc so far: weight = 450, fft = False, neurons = 150, d_rate = 0.5, layers = 6

        #BEST weights 600.0

        #BEST activation "relu" ["relu", "sigmoid"]
        #BEST neurons    150    [150.0, 900.0, 2000.0, 4000.0]

        #BEST neurons    150   [125, 150, 175, 200.0]
        #BEST layers     7   [3,4,5,6,7]

        #BEST neurons    150    [150, 160]
        #BEST d_rate     0.55   [0.45, 0.5, 0.55]

        #neurons   150      [150]
        #BEST fft  False   [True, False]



        if CV == True :
            one = [150]
            one_sz = len(one)
            two = [True, False]
            two_sz = len(two)

            skf = StratifiedKFold(n_splits = 5)
            scores = np.zeros((one_sz,two_sz))
            fold = 0
            for tr_i, te_i in skf.split(X_train, y_train) :
                fold += 1
                for i in range(one_sz) :
                    for j in range(two_sz) :
                        reset_keras(self._model)
                        filepath = f'./temp/weights{i}{j}.csv'
                        checkpoint = ModelCheckpoint(filepath, monitor = 'auc', verbose = 1, save_best_only = True, mode = 'max')
                        self._model = NNet(X_train.shape[1],neurons = one[i], act = "relu",
                            d_rate = 0.55, layers = 7, fft = two[j],
                            initial_bias = np.log(self._weights[1]/self._weights[0]))
                        print(f"Evaluating NN with {one[i]} neurons  and {two[j]} activations in fold {fold}.")
                        self._model.fit(X_train[tr_i], y_train[tr_i], class_weight = self._weights, epochs = 150,
                            batch_size = 64,
                            #validation_split=0.2,
                            callbacks = [checkpoint])
                        self._model.load_weights(filepath)
                        scores[i,j] += self.evaluate(y_train[te_i], self.predict(X_train[te_i]))
                        

            print("the score matrix for the CV is:")
            print(scores)
            a = np.unravel_index(scores.argmax(), scores.shape)
            reset_keras(self._model)
            self._model = NNet(X_train.shape[1],neurons = one[a[0]], act = "relu",
                layers = 7, d_rate = 0.55, fft =  two[a[1]],
                initial_bias = np.log(self._weights[1]/self._weights[0]))
            filepath = f'./temp/weights.csv'
            checkpoint = ModelCheckpoint(filepath, monitor = 'auc', verbose = 1, save_best_only = True, mode = 'max')
            
            self._model.fit(X_train, y_train, class_weight = self._weights, epochs = 300,
                batch_size = 128,
                #validation_split = 0.15,
                callbacks = [checkpoint])
            
            self._model.load_weights(filepath)
        
        else :
            self._model = NNet(X_train.shape[1],neurons = 150, act = 'relu',
                layers = 7, d_rate = 0.55, initial_bias = np.log(self._weights[0]/self._weights[1]))
            filepath = f'./temp/weights.csv'
            checkpoint = ModelCheckpoint(filepath, monitor = 'val_auc', verbose = 1, save_best_only = True, mode = 'max')
            if remember == False :
                self._model.fit(X_train, y_train, class_weight = self._weights, epochs = 1500,
                    batch_size = 128,
                    validation_split = 0.13,
                    callbacks = [checkpoint])
            
            self._model.load_weights(filepath)

    def predict(self,X_test, proba = True) :
        X_test = self.scaler.transform(X_test)
        y_pred = self._model.predict(X_test)
        prediction = []
        for i in y_pred :
            prediction.append(i[0])
        if proba == False :
            prediction = [int(i > self._threshold) for i in prediction]
        return prediction


class hybrid(model) :

    def __init__ (self, weights):
        self._weights = weights
        self._name = 'Hybrid'
        self._model = [NN(weights), svm(weights), XGB(weights)]
    
    def fit(self,X_train, y_train) :
        for m in self._model :
            m.fit(X_train, y_train)

    def predict(self,X_test, proba = True) :
        prediction = np.ones(X_test.shape[0])
        for m in self._model :
            if m._name == 'SVM' :
                new_pred = 1.0 / (1.0 + np.exp(-m.predict(X_test, proba)))
            else :
                new_pred = m.predict(X_test, proba)

            prediction = np.multiply(prediction,new_pred)
        return prediction
 

def load_dataframes(is_test = False, dropname = ['None'], industrydrop = False) :
    if is_test :
        istest = 'test'
    else :
        istest = 'train'
    if industrydrop :
        drop = '/industry_dropped'
    else :
        drop = ''

    df = pd.read_csv(f'./dta{drop}/{istest}/{1992}_company_data_{istest}.csv', header = [0,1])
    if dropname != ['None']:
        df = dropnames(df, 1992, dropname)
    arr1 = df.iloc[:,2:].to_numpy()
    for date in range(1993,2015):
        dfn = pd.read_csv(f'./dta{drop}/{istest}/{date}_company_data_{istest}.csv', header = [0,1])
        if dropname != ['None']:
            dfn = dropnames(dfn, date, dropname)
        newarray = dfn.iloc[:,2:].to_numpy()
        arr1 = np.append(arr1, newarray, axis = 0)
    if not is_test :
        np.random.shuffle(arr1)
    
    return arr1[:,1:], arr1[:,0]

#given a dataframe, a date, and an array of names,
#it returns the dataframe with all the names contained in the array dropped 
def dropnames(df, date, names) :
    for year in range(date, date+5) :
        current = str(year)
        for name in names:
            try:
                df.drop((current, name), axis = 1, inplace = True)
            except:
                continue
    return df

