import pandas as pd



#calculates the g-score and inserts it at the end of each row
#has to be calculated AFTER f-score
def g_score(df, starting_year) :
    #insert correct names for fixd assets & cmn stock
    fixed_assets = "Property, Plant And Equipment, Total - Gross"
    common_stock = "Common Stock, Total"
    for i in range(starting_year + 1, starting_year + 5) :
        year = str(i)
        df_last = df[str(i-1)]
        df_curr = df[year]

        #calculate  SGR
        df[year, "SGR"] = (df_curr["Total Revenue"] - df_last["Total Revenue"]) /df_last["Total Revenue"]
        #calculate RDINT
        df[year, "RDINT"] = df_curr["Research And Development"]/df_last["Total Assets, Reported"]
        #calculate CAPINT
        df[year, "CAPINT"] = df_curr["Capex, Discrete"]/df_last["Total Assets, Reported"]
        #calculate ADINT
        df[year, "ADINT"] = df_curr["Advertising Expense"]/df_last["Total Assets, Reported"]

        if i == starting_year + 4 :
            #prevent spread of NaN entries caused by 0-division
            df.fillna(0.00001618, inplace = True)
            md_sgr = (df[year, "SGR"] + df[str(i-1), "SGR"] + df[str(i-2), "SGR"] + df[str(i-3), "SGR"]) / 4.0
            #calculate VARSGR
            df[year,"VARSGR"] = ((df[year, "SGR"] - md_sgr)**2 + (df[str(i-1), "SGR"] - md_sgr)**2 +
                (df[str(i-2), "SGR"] - md_sgr)**2 + (df[str(i-3), "SGR"] - md_sgr)**2)
            #calculate VARROA
            md_roa = (df[year, "ROA"] + df[str(i-1), "ROA"] + df[str(i-2), "ROA"] + df[str(i-3), "ROA"]) / 4.0
            df[year,"VARROA"] = ((df[year, "ROA"] - md_roa)**2 + (df[str(i-1), "ROA"] - md_roa)**2 +
                (df[str(i-2), "ROA"] - md_roa)**2 + (df[str(i-3), "ROA"] - md_roa)**2)

            #calculate Medii
            medii = df[year].median(axis = 0)
            CFO_m = medii["CFO"]
            ROA_m = medii["ROA"]
            VARROA_m = medii["VARROA"]
            VARSGR_m = medii["VARSGR"]
            RDINT_m = medii["RDINT"]
            CAPINT_m = medii["CAPINT"]
            ADINT_m = medii["ADINT"]
            


            #calculate GSCORE
            df[year,"GSCORE"] = 0
            for j in range(df.shape[0]) :
                if df[year,"ROA"].iloc[j] > ROA_m :
                    df[year,"GSCORE"].iloc[j] += 1
                if df[year,"CFO"].iloc[j] > CFO_m :
                    df[year,"GSCORE"].iloc[j] += 1
                if df[year,"VARROA"].iloc[j] < VARROA_m :
                    df[year,"GSCORE"].iloc[j] += 1
                if df[year,"VARSGR"].iloc[j] < VARSGR_m :
                    df[year,"GSCORE"].iloc[j] += 1
                if df[year,"CFO"].iloc[j] >= df[year,"ROA"].iloc[j] :
                    df[year,"GSCORE"].iloc[j] += 1
                if df[year,"RDINT"].iloc[j] > RDINT_m :
                    df[year,"GSCORE"].iloc[j] += 1
                if df[year,"CAPINT"].iloc[j] > CAPINT_m :
                    df[year,"GSCORE"].iloc[j] += 1
                if df[year,"ADINT"].iloc[j] > ADINT_m :
                    df[year,"GSCORE"].iloc[j] += 1




    return df




                


        
