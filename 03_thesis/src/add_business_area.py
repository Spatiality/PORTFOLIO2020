import pandas as pd
import csv
import eikon as ek

import numpy as np

def add_business_area() :
    data_items = ['TR.BusinessSector']

    ek.set_app_key(open("app_key.txt", "r").read())

    business_areas = []

    companies = {}

    def strip_ending(string) :
        if string == None :
            return "None"
        if len(string) <= 6 :
            return string
        if " - NEC" == string[-6:] :
            return string[:-6]
        else :
            return string

    #df, err = ek.get_data(['SCREEN(U(IN(Equity(active,public,primary))),Contains(TR.CommonName,"Wesco "))'], ['TR.CommonName']) 

    for date in range(1992,2015) :

        co_list = pd.read_csv(f'../{str(date)}_company_data.csv',header = [0,1])

        co_names = list(co_list[str(date),'Instrument'].values)

        co_data, err = ek.get_data(co_names, data_items, parameters = {"SDate": 'FY'+str(date+4)})

        size = co_data.shape[0]

        for i in range(size) :
            area = strip_ending(co_data.iloc[i,1])
            company = co_data.iloc[i,0]
            if area not in business_areas :
                business_areas.append(area)
            if company not in companies :
                companies[company] = [area]
            elif area not in companies[company]:
                companies[company].append(area)

    for date in range(1992,2015) :

        co_list = pd.read_csv(f'../{str(date)}_company_data.csv', header = [0,1])

        size = co_list.shape[0]
        width = co_list.shape[1]

        for area in business_areas :
            co_list[str(date),area] = np.zeros(size)
        #print(co_list)
        for i in range(size) :
            
            for j in range(len(business_areas)) :
                if business_areas[j] in companies[co_list.iloc[i,0]] :
                    co_list.loc[ i, (str(date), business_areas[j])] = 1.0

                    #co_list.iloc[i,width + j] = 1.0
        #print(co_list)
        co_list.to_csv(f'{str(date)}_company_data.csv', index = False)

        
