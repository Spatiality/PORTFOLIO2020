import pandas as pd



#calculates the m-score and inserts it at the end of each row
#has to be calculated AFTER g-score
def m_score(df, starting_year) :
    #insert correct name for operating income
    operating_income = "Operating Income"
    for i in range(starting_year + 1, starting_year + 5) :
        year = str(i)
        df_last = df[str(i-1)]
        df_curr = df[year]

        #calculate  DSRI
        df[year, "DSRI"] = ((df_curr["Accounts Receivable - Trade, Net"]/df_curr["Total Revenue"])
            / (df_last["Accounts Receivable - Trade, Net"]/df_last["Total Revenue"]))
        #calculate GMI
        df[year, "GMI"] = (((df_last["Total Revenue"]- df_last["Cost of Revenue, Total"])/df_last["Total Revenue"])
            /((df_curr["Total Revenue"]- df_curr["Cost of Revenue, Total"])/df_curr["Total Revenue"]))
        #calculate AQI
        df[year, "AQI"] = (((1-df_curr["Total Current Assets"]
            -df_curr["Property, Plant And Equipment, Total - Gross"])/df_curr["Total Revenue"]) /
            ((1-df_last["Total Current Assets"]
            -df_last["Property, Plant And Equipment, Total - Gross"])/df_last["Total Revenue"]))
        #calculate SGI
        df[year, "SGI"] = df_curr["Total Revenue"]/df_last["Total Revenue"]
        #calculate DEPI
        df[year, "DEPI"] = ((df_last["Depreciation And Amortization"]/(df_last["Depreciation And Amortization"]
            +df_last["Property, Plant And Equipment, Total - Gross"]))/(df_curr["Depreciation And Amortization"]
            /(df_curr["Depreciation And Amortization"]+df_curr["Property, Plant And Equipment, Total - Gross"])))
        #calculate SGAI
        df[year, "SGAI"] = ((df_curr["Selling/General/Administrative Expense, Total"]/df_curr["Total Revenue"])
            /(df_last["Selling/General/Administrative Expense, Total"]/df_last["Total Revenue"]))
        #calculate TATA
        df[year, "TATA"] = ((df_curr[operating_income] - df_curr["$flo"])/df_curr["Total Assets, Reported"])
        #calculate LGVI
        df[year,"LGVI"] = (((df_curr["Total Long Term Debt"] + df_curr["Total Current Liabilities"])
            /df_curr["Total Assets, Reported"])/((df_last["Total Long Term Debt"]
            + df_last["Total Current Liabilities"])/df_last["Total Assets, Reported"]))
        df.fillna(0.0)
        #calculate M-Score
        df[year, "MSCORE"] = (-4.840 + df[year,"DSRI"]*0.920 + df[year,"GMI"]*0.528 + df[year,"AQI"]*0.404
            + df[year,"SGI"]*0.892 + df[year,"DEPI"]*0.115 - df[year,"SGAI"]*0.127
            + df[year,"TATA"]*4.679 - df[year,"LGVI"]*0.327)




    return df




                


        
