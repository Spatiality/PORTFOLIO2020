import pandas as pd
import numpy as np


#calculates the o-score and inserts it at the end of each row
#has to be calculated AFTER z-score

def o_score(df, starting_year) :
    #insert correct names for fixd assets & cmn stock
    fixed_assets = "Property, Plant And Equipment, Total - Gross"
    common_stock = "Common Stock, Total"
    operating_income = "Operating Income"

    for i in range(starting_year + 1, starting_year + 5) :
        year = str(i)
        df_last = df[str(i-1)]
        df_curr = df[year]
        df[year, "WCTA"] = (df_curr["Total Current Assets"] - df_curr["Total Current Liabilities"])/df_curr["Total Assets, Reported"]
        df[year,"Size"] = np.log(df_last["Total Assets, Reported"].values) - np.log(721.57)
        df[year, "TLTA"] = df_curr["Total Liabilities"]/df_curr["Total Assets, Reported"]
        df[year, "CLCA"] = df_curr["Total Current Liabilities"]/df_curr["Total Current Assets"]
        df[year, "NITA"] = df_curr["Net Income Incl Extra Before Distributions"]/df_curr["Total Assets, Reported"]
        df[year, "FUTL"] = df_curr["$flo"]/df_curr["Total Liabilities"]
        NI  = df_curr["Net Income Incl Extra Before Distributions"]
        NI1 = df_last["Net Income Incl Extra Before Distributions"]
        df[year, "CHIN"] = (NI.values - NI1.values)/(abs(NI.values) + abs(NI1.values))
        df[year, "OENEG"] = 0.0
        df.loc[NI < 0.0,(year, "OENEG")] = 1.0
        df.loc[NI1 > 0.0,(year, "OENEG")] *= 0.0
        
        df[year, "INTWO"] = 0.0
        df.loc[df_curr["Total Liabilities"] > df_curr["Total Assets, Reported"],(year, "INTWO")] = 1.0
        
        df.fillna(0.0000168, inplace = True)
        
        df[year, "O-score"] = (-1.32 - 0.407*df[year,"Size"] + 6.03*df[year, "TLTA"] - 1.43*df[year, "WCTA"]
            + 0.0757*df[year, "CLCA"] - 1.72*df[year, "OENEG"] - 2.37*df[year, "NITA"] - 1.83*df[year, "FUTL"]
            +0.285*df[year, "INTWO"] - 0.521*df[year, "CHIN"])
    
    return df


                


        
