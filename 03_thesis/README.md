
F Schweizers Bachelor Thesis:
A foray into machine-learning-powered stock selection

Authors: Florian Schweizer

Prerequisites:
Python 3
scikit-learn
Keras
NumPy
pandas
Eikon Refinitiv Python API

usage:
. To collect Data, connect the Eikon Refinitiv API proxy, then run 'Python3 gather_data.py'
