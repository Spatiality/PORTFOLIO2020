import pandas as pd
import numpy as np
from src.models import model, load_dataframes, svm, XGB, NN, hybrid
from src.stats import correlation_matrix, descr_stats, variable_importance, get_col_names, class_averages, principal_components

droplist = ["ROA","$flo","CFO","ACCRUAL","DLIQUID","DTURN","DROA","FSCORE"]

indnames, names = get_col_names()

droplist = indnames

if __name__ == '__main__' :
    
    #load files into one single np array that disregards the numbering by year
    
    #X_train, y_train = load_dataframes(is_test = False, industrydrop = True)
    #X_test, y_test = load_dataframes(is_test = True, industrydrop = True)
    #principal_components(three_d = True)
    

    X_train, y_train = load_dataframes(is_test = False)
    X_test, y_test = load_dataframes(is_test = True)


    #define class weights
    #weights = {0:1.00, 1:1000.0}
    unique, counts = np.unique(y_train, return_counts=True)
    weights = dict(zip(unique, 1.0/counts))
    models = [ model(weights), svm(weights), XGB(weights), NN(weights), hybrid(weights)]
    
    #training and evaluation of different models
    """
    #m = NN(weights)
    #m = XGB(weights)
    m = svm(weights)
    #m = model(weights)
    #m = hybrid(weights)
    #fit model
    
    m.fit(X_train, y_train, CV = True)

    #predict values

    y_pred = m.predict(X_test)


    #evaluate prediction

    #print(y_pred)

    m.optimal_threshold(y_test, y_pred)


    m.display_selected(y_pred)

    #m.evaluate(y_test, y_pred)

    m.plot_roc(y_test,y_pred)
    m.show()
    m.clear_plot()

    
    #m.accuracy(y_test, y_pred, show = True)
    m.show_PCA(y_test, y_pred, three_d = True)
    m.show()
    m.clear_plot()
    m.show_PCA(y_test, y_pred, dim1 = 0, dim2 = 1)
    m.show()
    m.clear_plot()
    m.show_PCA(y_test, y_pred, dim1 = 0, dim2 = 2)
    m.show()
    m.clear_plot()
    m.show_PCA(y_test, y_pred, dim1 = 1, dim2 = 2)
    m.show()
    """

    #Descriptive statistics on the dataset
    
    #descr_stats(stddev = False)   
    #correlation_matrix()
    #variable_importance()
    #class_averages()
    #principal_components()

        
    #comparison of different models on single ROC plot
    
    
    for m in models :   

        #fit model
    
        m.fit(X_train, y_train)

        
        #predict values
        
        y_pred = m.predict(X_test)
        
        m.optimal_threshold(y_test, y_pred)
        #evaluate prediction

        #m.evaluate(y_test,y_pred)
        

        results = m.display_selected(y_pred)

        results.to_csv(f'selected_firms_{m._name}.csv', index = False)

        #m.plot_roc(y_test,y_pred)

    #models[0].show()
    
