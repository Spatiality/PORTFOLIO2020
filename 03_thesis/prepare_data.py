from src.clean_data import clean_data
from src.f_score import f_score
from src.g_score import g_score
from src.m_score import m_score
from src.z_score import z_score
from src.o_score import o_score
from src.QMJ_ratios import QMJ
from src.other_ratios import calculate_ratios
from src.models import dropnames
from src.stats import get_col_names
import pandas as pd
import numpy as np

droplist = ["ROE", "debt_to_assets", "current_ratio", "acidity", "TIE", "invtry turnover",
    "fixed_asset_turnover","LEVER","Gross Margin",
    "ROA","$flo","CFO","ACCRUAL","DLIQUID","DTURN","DROA","FSCORE",
    "DLEVER","ISSUES","DMARGIN","SGR","RDINT","CAPINT","ADINT","VARSGR","VARROA","GSCORE",
    "DSRI","GMI","AQI","SGI","DEPI","SGAI","TATA","LGVI","MSCORE",
    "X1","X2","X3","X4","X5","ZSCORE",
    "WCTA","Size","TLTA","CLCA","NITA","FUTL","CHIN","OENEG","INTWO","O-score",
    "DGMAR", "DGPOA", "DROE", "D_ROA", "D$FLO", "QMJ_PROFIT", "QMJ_GROWTH", "QMJ_SAFETY", "QMJ"]

indnames, names = get_col_names()

drops = "LBTYK.OQ"

if __name__ == '__main__' :
    
    nonpicks = 0
    picks = 0

    for date in range(1992,2015) :

        co_list = pd.read_csv(f'./dta/{str(date)}_company_data.csv', header = [0,1])
        #co_list.drop("Unnamed: 0_level_0", axis = 1, inplace = True)

        
        co_list.to_csv(f'./dta/old/{str(date)}_company_data.csv', index = False)
        co_list.replace([0.00001618, 0.00000168, 0.0000168], np.nan, inplace = True)
        co_list = dropnames(co_list, date, droplist)
        
        selection = co_list[str(date), "Instrument"] == drops
        try :
            co_list.drop(co_list[selection], axis = 0, inplace = True)
        except:
            pass
        selection = co_list.isnull().sum(axis=1) > 0.35*(len(co_list.columns) - len(indnames) - 2)
        co_list.drop(co_list[selection].index, axis = 0, inplace = True)
        co_list.fillna(0.00001618, inplace = True)
        contents = co_list[str(date), "Picked"].value_counts()
        nonpicks+= contents.iloc[0]
        try : 
            picks += contents.iloc[1]
        except:
            pass
        
        #co_list = clean_data(co_list, date)
        co_list = f_score(co_list, date)
        co_list = g_score(co_list, date)
        co_list = z_score(co_list, date)
        co_list = o_score(co_list,date)
        co_list = m_score(co_list, date)
        co_list = calculate_ratios(co_list, date)
        co_list = QMJ(co_list, date)
        

        co_list.replace([np.inf, -np.inf, "NaN", "inf", "-inf"], np.nan, inplace = True)
        co_list.fillna(0.00001618, inplace = True)
        print(co_list)
        co_list.to_csv(f'./dta/{str(date)}_company_data.csv', index = False)
        
        print(f'All data processed for the year {date}')
    print(f'We have in total {picks} picks and {nonpicks} nonpicks!')
