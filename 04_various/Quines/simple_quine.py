# a simple quine for python connoisseurs
other_code = "somewhere, there's an interpreter that can & will run with this input"
code = """
print('# a simple quine for python connoisseurs')
print(f'other_code = "{other_code}"')
print('code = ' + 3*'"', end = '')
print(code + 3*'"')
print(code)"""

print('# a simple quine for python connoisseurs')
print(f'other_code = "{other_code}"')
print('code = ' + 3*'"', end = '')
print(code + 3*'"')
print(code)
