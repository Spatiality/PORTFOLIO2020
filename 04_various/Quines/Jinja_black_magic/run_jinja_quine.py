from jinja2 import Template

with open('indefinitely_growing_template.jinja', 'r') as templtfile2:
  yquine = templtfile2.read()
  for i in range(3):
    yquine = Template(yquine).render(content = f"'this is comment no. {i+1}'", finish = False)
    print(yquine)
  yquine = Template(yquine).render(finish = True)
  print(yquine)
