#include <iostream>

template<size_t I>
struct meta_fac {
	enum {
		result = I*meta_fac<I-1>::result
	};
};

template<>
struct meta_fac<1> {
	enum {
		result = 1
	};
};


template<size_t N, size_t K, size_t I, bool A>
struct meta_bin {
	enum {
		result = 1
	};
};

template<size_t N,size_t K,size_t I>
struct meta_bin<N,K,I, true> {
	enum {
		result = I * meta_bin<N,K,I-1,(I-1>(N-K))>::result
	};
};





template<size_t N,size_t K, size_t I>
struct meta_bin<N,K, I,false> {
	enum { result = 1
	};
};



		
template<size_t N, size_t K>
struct binomial {
	
		enum {
			result = (meta_bin<N,K,N,(N>N-K)>::result / meta_fac<K>::result)
		};
};



int main() {
	std::cout << meta_fac<10>::result << " = 10! ; 20! = " << meta_fac<20>::result
		  << "\n";

        std::cout << binomial<6,2>::result << " = 6 choose 2 " << std::endl;

	return 0;
}
