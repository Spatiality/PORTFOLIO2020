An assignment for Scientific Programming techniques:

To let the compiler generate arbitrarily many lines of code,
as example a precompiled faculty, or a precompiled binomial.

Authors: Florian Schweizer

Prerequisites: C++ compiler

usage: run 'make'
