#include<iostream>
#include<fstream>
#include<ctime>
#include<random>
#include<vector>
#include <cmath>

double SpeedCreator(double normalSpeed) {
	std::random_device rd;  
	std::mt19937 gen(rd());
	// right skew distribution for speeds
	std::chi_squared_distribution<double> chi(3.5);
	return (chi(gen) + 1.0 )*normalSpeed/2.0;
}

void MeshRing(int Nnodes) {
	int originalNNodes = Nnodes;
	std::random_device rd;  
	std::mt19937 gen(rd());
	std::uniform_real_distribution<double> u(0.0,1.0);
	int Link_count = 0;
	double SpeedN = 15;
	std::uniform_int_distribution<int> next(0,Nnodes-1);

	//initialising Adjacency Matrix
	std::vector<double> Empty(Nnodes,0);
	std::vector<std::vector<double>> AdjacencyMatrix(Nnodes);
	for(int i = 0;i<Nnodes;i++) {
		AdjacencyMatrix[i] = Empty;
	}
	double Speed;

	//constructing a simple ring
	Speed = SpeedCreator(SpeedN);
	AdjacencyMatrix[0][Nnodes-1] = Speed;
	AdjacencyMatrix[Nnodes-1][0] = Speed;
	Link_count+=2;

	for (int i = 0;i<Nnodes-1;i++) {
		Speed = SpeedCreator(SpeedN);
		AdjacencyMatrix[i][i+1] = Speed;
		AdjacencyMatrix[i+1] [i] = Speed;
		Link_count+=2;
		Speed = SpeedCreator(20*SpeedN);
		AdjacencyMatrix [i][i] = Speed;
	}
	Speed = SpeedCreator(20*SpeedN);
	AdjacencyMatrix [Nnodes-1][Nnodes-1] = Speed;

	while (SpeedN >0.5) {
		SpeedN*=.9;
		for (int i = 0;i<Nnodes;i++) {
			int j = next(gen);
			while(AdjacencyMatrix[i][j]>0) //link already taken?
				j= next(gen); //reroll!
			Speed = SpeedCreator(SpeedN);
			if(u(gen)>(SpeedN/1.4)) {
				AdjacencyMatrix[i][j] = Speed;
				AdjacencyMatrix[j][i] = Speed;
				Link_count+=2;
			}
		}
	}
	//Now write to file
	//
	//
	//

	std::ofstream file;
	file.open("Mesh_Ring.out");

	file << Nnodes << ' ' << Link_count << '\n';
	double temp = 0;
	for (int i = 0; i<Nnodes;i++) {
		for (int j = 0; j< Nnodes;j++) {
			temp = AdjacencyMatrix[i][j];
			if(temp > 0 ) {
			       file << i << ' ' << j << ' ' << 	AdjacencyMatrix[i][j] << std::endl;
			}
		}
	}

	std::cout << "Network written for "
		  << Nnodes << " Nodes and "
		  << Link_count << " Links."
		  << std::endl << std::endl;

}


//Creates a file that resembles a firm-wide bus or a supercomputer structure; has a Bus-Grid-Star topology
void ModelBus(int Nnodes) {
	int warning = 0;
	int originalNNodes = Nnodes;
	int toplNodes = cbrt(Nnodes);
	std::random_device rd;  
	std::mt19937 gen(rd());
	//top level random numbers
	std::uniform_int_distribution<int> top(toplNodes-1,toplNodes+1);

	toplNodes = top(gen);
	std::vector<std::vector<int>> Structure;
	Nnodes -=toplNodes;
	int midlNodes = Nnodes/(4*toplNodes);
	std::uniform_int_distribution<int> middle(midlNodes-6 > 0 ? midlNodes-6 : 1,
						  midlNodes+6 > 13 ? midlNodes+6 : 2* midlNodes );
	midlNodes = 0;
	//create a list with the number of nodes connected to top-level bus
		for (int i=0;i<toplNodes;i++) {
		int length = middle(gen);
		std::vector<int> mid(length);
		Structure.push_back(mid);
		midlNodes +=length;
	}
	Nnodes -=midlNodes;
	int llNodes = Nnodes/midlNodes;
	std::uniform_int_distribution<int> bottom(llNodes-1 > 0 ? llNodes-1 : 1,
						  llNodes+1 > 3 ? llNodes+1 : llNodes*2 );
	llNodes = 0;
	//fill the list with number of lower-level Nodes
	for (int i = 0;i<toplNodes;i++) {
		for (int & lowNode : Structure[i]) {
			lowNode = bottom(gen);
			Nnodes -=lowNode;
			llNodes+=lowNode;
		}
	}
	Nnodes = originalNNodes - Nnodes;
	//initialising Adjacency Matrix
	std::vector<double> Empty(Nnodes,0);
	std::vector<std::vector<double>> AdjacencyMatrix(Nnodes);
	for(int i = 0;i<Nnodes;i++) {
		AdjacencyMatrix[i] = Empty;
	}
	//fill the Bus Level
	
	double SpeedN = 150;
	int Link_count = 0;
	double Speed = SpeedCreator(SpeedN);
	for (int i = 0;i<toplNodes-1;i++) {
		Speed = SpeedCreator(SpeedN);
		AdjacencyMatrix[i][i+1] = Speed;
		AdjacencyMatrix[i+1][i] = Speed;
		Link_count+=2;
	}

	// fill grid level
	
	SpeedN/=10;
	int count_i = 0;
	int count_j = toplNodes;
	//ensure connections to top levels
	for(std::vector<int> i: Structure) {
		int length = i.size();
		for(int j = 0;j<6&&j<length;j++) {
			Speed = SpeedCreator(SpeedN);
			if(AdjacencyMatrix[count_i][count_j]==0)
				Link_count+=2;
			AdjacencyMatrix[count_i][count_j] = Speed;
			AdjacencyMatrix[count_j][count_i] = Speed;
			count_j++;
		
		}
		count_i++;
		count_j+= (length>6 ? length - 6 : 0);
	}
	//connect grids
	count_i = toplNodes;
	count_j = toplNodes;
	int pattern = 0;
	for(std::vector<int> i: Structure) {
		int count = 0; //counts number of nodes visited
		int length = i.size(); //number of nodes to visit
		Speed = SpeedCreator(SpeedN);
		if(AdjacencyMatrix[count_i][count_j+5]==0) {
			Link_count+=2;
			AdjacencyMatrix[count_i][count_j+5] = Speed;
			AdjacencyMatrix[count_j+5][count_i] = Speed;
		} else warning++;
		while(count<6&&count<length-1) {
			Speed = SpeedCreator(SpeedN);
			if(AdjacencyMatrix[count_i][count_j+1]==0) {
				Link_count+=2;
				AdjacencyMatrix[count_i][count_j+1] = Speed;
				AdjacencyMatrix[count_j+1][count_i] = Speed;
			} else warning++;
			if(length>=count+6) {
				Speed = SpeedCreator(SpeedN);
				if(AdjacencyMatrix[count_i][count_j+6]==0) {
					Link_count+=2;
					AdjacencyMatrix[count_i][count_j+6] = Speed;
					AdjacencyMatrix[count_j+6][count_i] = Speed;
				} else warning++;
				
			}
			count++;
			count_i++;
			count_j++;
		}

		int spacing= 6;
		int patterning = 12;
		while(count<length) {
			if (pattern ==0) {
				if (count%2==0) {

					Speed = SpeedCreator(SpeedN);
					if(AdjacencyMatrix[count_i-spacing][count_j]==0) {
						Link_count+=2;
						AdjacencyMatrix[count_i-spacing][count_j] = Speed;
						AdjacencyMatrix[count_j][count_i-spacing] = Speed;
					} else warning++;

					if(patterning%spacing!=0) {
						Speed = SpeedCreator(SpeedN);
						if(AdjacencyMatrix[count_j][count_j-1]==0) {
							AdjacencyMatrix[count_j][count_j-1] = Speed;
							AdjacencyMatrix[count_j-1][count_j] = Speed;
							Link_count+=2;
						} else warning++;
					}
	
					count_j++;
					count++;
					count_i++;
					spacing++;

				} else {
					if(AdjacencyMatrix[count_i-spacing][count_j]==0) {
						Speed = SpeedCreator(SpeedN);
						AdjacencyMatrix[count_i-spacing][count_j] = Speed;
						AdjacencyMatrix[count_j][count_i-spacing] = Speed;
						Link_count+=2;
					} else warning++;
					count++;
					count_j++;
 					count_i++;
					if(count/patterning==2) {
						pattern = 1;
						if(AdjacencyMatrix[count_i-spacing][count_j-1]==0) {
							Speed = SpeedCreator(SpeedN);
							AdjacencyMatrix[count_i-spacing][count_j-1] = Speed;
							AdjacencyMatrix[count_j-1][count_i-spacing] = Speed;
							Link_count+=2;
						} else warning++;
					}
				}

			} else {
				if(AdjacencyMatrix[count_i-spacing][count_j]==0) {
					Speed = SpeedCreator(SpeedN);
					AdjacencyMatrix[count_i-spacing][count_j] = Speed;
					AdjacencyMatrix[count_j][count_i-spacing] = Speed;
					Link_count+=2;
				} else warning++;
				count++;
				count_j++;
 				count_i++;


				if(count/patterning==3) {
					pattern = 0;
					patterning*=2;
				}
			}



		}
		

	}

	//fill base level
	
	SpeedN/=10;
	count_i = toplNodes;
	count_j = toplNodes + midlNodes;
	for(int i = 0;i<toplNodes;i++) {
		for(int j : Structure[i]) {
			for (int k = 0;k<j;k++) {
				Speed = SpeedCreator(SpeedN);
				if(AdjacencyMatrix[count_i][count_j]==0) {
					AdjacencyMatrix[count_i] [count_j] = Speed;
					AdjacencyMatrix[count_j] [count_i] = Speed;
					Link_count+=2;
				} else warning++;
				AdjacencyMatrix[count_j] [count_j] = 100*SpeedCreator(SpeedN);
				count_j++;

			}
			count_i++;
		}
	}
	//Now write to file
	//
	//
	//

	std::ofstream file;
	file.open("Model_Bus.out");

	file << Nnodes << ' ' << Link_count << '\n';
	double temp = 0;
	for (int i = 0; i<Nnodes;i++) {
		for (int j = 0; j< Nnodes;j++) {
			temp = AdjacencyMatrix[i][j];
			if(temp > 0 ) {
			       file << i << ' ' << j << ' ' << 	AdjacencyMatrix[i][j] << std::endl;;
			}
		}
	}
//	file << warning << " Collisions detected.";

	std::cout << "Bus network written for "
		  << Nnodes << " Nodes and "
		  << Link_count << " Links."
		  << std::endl << std::endl;

}



//Creates a file of a tiny model internet with the Ring-Star-Star topology, given a number of nodes
void ModelInternet(int Nnodes) {
	int originalNNodes = Nnodes;
	int toplNodes = cbrt(Nnodes);
	std::random_device rd;  
	std::mt19937 gen(rd());
	//top level random numbers
	std::uniform_int_distribution<int> top(toplNodes-1,toplNodes+1);

	toplNodes = top(gen);
	std::vector<std::vector<int>> Structure;
	Nnodes -=toplNodes;
	int midlNodes = sqrt(Nnodes)/toplNodes;
	std::uniform_int_distribution<int> middle(midlNodes-3 > 0 ? midlNodes-3 : 1,
						  midlNodes+3 > 6 ? midlNodes+3 : 2* midlNodes );
	midlNodes = 0;
	//create a list with the number of nodes connected to top-level ring
	for (int i=0;i<toplNodes;i++) {
		int length = middle(gen);
		std::vector<int> mid(length);
		Structure.push_back(mid);
		midlNodes +=length;
	}
	Nnodes -=midlNodes;
	int llNodes = Nnodes/midlNodes;
	std::uniform_int_distribution<int> bottom(llNodes-9 > 0 ? llNodes-9 : 1,
						  llNodes+9 > 18 ? llNodes+9 : llNodes*2 );
	llNodes = 0;
	//fill the list with number of lower-level Nodes
	for (int i = 0;i<toplNodes;i++) {
		for (int & lowNode : Structure[i]) {
			lowNode = bottom(gen);
			Nnodes -=lowNode;
		}
	}
	Nnodes = originalNNodes - Nnodes;
	//initialising Adjacency Matrix
	std::vector<double> Empty(Nnodes,0);
	std::vector<std::vector<double>> AdjacencyMatrix(Nnodes);
	for(int i = 0;i<Nnodes;i++) {
		AdjacencyMatrix[i] = Empty;
	}
	//fill the Circle Level
	
	double SpeedN = 150;
	int Link_count =  2;
	double Speed = SpeedCreator(SpeedN);
	AdjacencyMatrix[0][toplNodes - 1] = Speed;
	AdjacencyMatrix[toplNodes - 1][0] = Speed;

	for (int i = 0;i<toplNodes-1;i++) {
		Speed = SpeedCreator(SpeedN);
		AdjacencyMatrix[i][i+1] = Speed;
		AdjacencyMatrix[i+1][i] = Speed;
		Link_count+=2;
	}	

	// fill mid level
	
	SpeedN/=10;
	int count_i = 0;
	int count_j = toplNodes;
	for(std::vector<int> i: Structure) {
		int length = i.size();
		for(int j = 0; j < length;j++) {
			Speed = SpeedCreator(SpeedN);
			AdjacencyMatrix[count_i] [count_j] = Speed;
			AdjacencyMatrix[count_j] [count_i] = Speed;
			count_j++;
			Link_count+=2;

		}
		count_i++;
	}

	//fill base level
	
	SpeedN/=10;
	count_i = toplNodes;
	count_j = toplNodes + midlNodes;
	for(int i = 0;i<toplNodes;i++) {
		for(int j : Structure[i]) {
			for (int k = 0;k<j;k++) {
				Speed = SpeedCreator(SpeedN);
				AdjacencyMatrix[count_i] [count_j] = Speed;
				AdjacencyMatrix[count_j] [count_i] = Speed;
				AdjacencyMatrix[count_j] [count_j] = 100*SpeedCreator(SpeedN);
				count_j++;
				Link_count+=2;
			}
			count_i++;
		}
	}
	//Now write to file
	//
	//
	//

	std::ofstream file;
	file.open("Model_Internet.out");

	file << Nnodes << ' ' << Link_count << '\n';
	double temp = 0;
	for (int i = 0; i<Nnodes;i++) {
		for (int j = 0; j< Nnodes;j++) {
			temp = AdjacencyMatrix[i][j];
			if(temp > 0 ) {
			       file << i << ' ' << j << ' ' << 	AdjacencyMatrix[i][j] << '\n';
			}
		}
	}

	std::cout << "Internet model network written for "
		  << Nnodes << " Nodes and "
		  << Link_count << " Links."
		  << std::endl << std::endl;

}





		






	



int main() {
	int InternetNodes = 0;
	int BusNodes = 0;
	
	std::cout << "Please enter the approximate number of nodes you'd like your model internet to have! " << std::endl;

	std::cin >> InternetNodes;

	ModelInternet(InternetNodes);

	std::cout << "Please enter the approximate number of nodes you'd like your model bus network to have! " << std::endl;

	std::cin >> BusNodes;


	ModelBus(BusNodes); 

	std::cout << "Please enter the approximate number of nodes you'd like your Mesh Ring network to have! " << std::endl;

	std::cin >> BusNodes;


	MeshRing(BusNodes);

	return 0;
}
