A partial assignment for Computational Social Science:

To make a model of different network topologies with randomly assigned transmission capacities,
generating links between nodes randomly while still adhering to the structural characteristics of the described topology

Authors: Florian Schweizer

Prerequisites: C++ compiler, Make

usage: run 'make'
