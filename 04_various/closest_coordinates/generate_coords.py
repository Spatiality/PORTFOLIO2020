import numpy as np
import random
import sys

args = sys.argv

coords = 5000000
if len(sys.argv) > 1 :
  coords = int(sys.argv[1])

y_range = 100000.0
x_range = 100000.0


with open('Random_coordinates.txt', 'w') as coordfile :
  coordar = np.zeros(shape = (coords, 2))
  for i in range(coords) :
    coordar[i][0] = random.uniform(-x_range, x_range)
    coordar[i][1] = random.uniform(-y_range, y_range)
    coordfile.write(str(coordar[i][0]) + '\n')
    coordfile.write(str(coordar[i][1]) + '\n')
  #and now the target coordinates to make it interesting.
  coordfile.write(str(coordar[:][0].mean()) + '\n')
  coordfile.write(str(coordar[:][1].mean()))