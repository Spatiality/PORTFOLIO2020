#include <cmath>
#include <iostream>
#include <cstdint>
#include <omp.h>
#include <string>
#include <chrono>
#include <vector>
#include <fstream>
#include <iterator>

double sqrtQuake( double number ) {
  double y = number;
  double x2 = y * 0.5;
  std::int64_t i = *(std::int64_t *) &y;
  // The magic number for doubles is from https://cs.uwaterloo.ca/~m32rober/rsqrt.pdf
  i = 0x5fe6eb50c7b537a9 - (i >> 1);
  y = *(double *) &i;
  y = y * (1.5 - (x2 * y * y));   // 1st iteration
  //y = y * ( 1.5 - ( x2 * y * y ) );   // 2nd iteration
  return y * number;
}

struct coord {

  double x;
  double y;

  void print() {
    std::cout << "x: " << x << " y: " << y << std::endl;
  }

};

double calc_dist(coord a, coord b) {
  double x_diff = a.x - b.x;
  double y_diff = a.y - b.y;
  double dist = x_diff*x_diff + y_diff*y_diff;
  return std::sqrt(dist);
}

double fastcalc(coord a, coord b) {
  double x_diff = a.x - b.x;
  double y_diff = a.y - b.y;
  double dist = x_diff*x_diff + y_diff*y_diff;
  return sqrtQuake(dist);
}

//The naive implementation just goes through the coordinate array
//and calculates the euclidean distance for every candidate

coord find_nearest_naive(coord* coordinates, coord target, int len) {
  double minlen = calc_dist(coordinates[0], target);
  int toplocation = 0;
  for(int i = 1; i<len; i++) {
    double tempdist = calc_dist(coordinates[i], target);
    if(minlen>tempdist) {
      minlen = tempdist;
      toplocation = i;
    }
  }
  return coordinates[toplocation];
}

struct dist {
  coord location;

  dist(coord l) {
    location = l;
  }

  double there(coord target) {
    return calc_dist(target, location);
  }

  double fast(coord target) {
    return fastcalc(target, location);
  }

};

coord find_naive_opt(coord* coordinates, coord target, int len) {
  double minlen = fastcalc(coordinates[0], target);
  int toplocation = 0;
  for(int i = 1; i<len; i++) {
    double tempdist = fastcalc(coordinates[i], target);
    if(minlen>tempdist) {
      minlen = tempdist;
      toplocation = i;
    }
  }
  return coordinates[toplocation];
}


coord find_nearest_optimized(coord* coordinates, coord target, int len) {

  double minl2 = calc_dist(coordinates[0], target);
  int toplocation = 0;

  for (int i = 1;i<len; i++) {
    double x_mintemp = abs(coordinates[i].x - target.x);
    if (x_mintemp < minl2) {
      double y_mintemp = abs(coordinates[i].y - target.y);
      if (y_mintemp < minl2) {
        //this effectively reduces the computation-intensive task to the case
        //that the coordinate is located in a square centered on the target
        //whose sides are of the length 2*Min_L2_distance, meaning the (iid) probability
        //that the coordinate is nearer to the target than the current best coordinate
        // is PI/4.0, or around ~ 0.785
        //We could further avoid the expensive sqrt computation by checking if
        //y_temp + x_temp < sqrt(2)*Min_L2_distance, meaning that we would shave off
        //the corners of the square to make it an octagon, further upping the
        //probability of a successful candidate to .948
        //A performance gain from this is probably compiler- / architecture-dependent
        double templ2 = calc_dist(coordinates[i], target);
        if (templ2 < minl2) {
         toplocation = i;
         minl2 = templ2;
        }
      }
    }
  }
  return coordinates[toplocation];
}

coord find_nearest_optimized_v2(coord* coordinates, coord target, int len) {

  double minl2 = calc_dist(coordinates[0], target);
  int toplocation = 0;
  double sqrt2 = std::sqrt(2.0);

  for (int i = 1;i<len; i++) {
    double x_mintemp = abs(coordinates[i].x - target.x);
    if (x_mintemp < minl2) {
      double y_mintemp = abs(coordinates[i].y - target.y);
      if (y_mintemp < minl2) {
        //this effectively reduces the computation-intensive task to the case
        //that the coordinate is located in a square centered on the target
        //whose sides are of the length 2*Min_L2_distance, meaning the (iid) probability
        //that the coordinate is nearer to the target than the current best coordinate
        // is PI/4.0, or around ~ 0.785
        //We could further avoid the expensive sqrt computation by checking if
        //y_temp + x_temp < sqrt(2)*Min_L2_distance, meaning that we would shave off
        //the corners of the square to make it an octagon, further upping the
        //probability of a successful candidate to .948
        //A performance gain from this is probably compiler- / architecture-dependent
        if (y_mintemp + x_mintemp < sqrt2*minl2) {
          double templ2 = calc_dist(coordinates[i], target);
          if (templ2 < minl2) {
           toplocation = i;
           minl2 = templ2;
          }
        }
      }
    }
  }
  return coordinates[toplocation];
}


coord find_nearest_parallel(coord* coordinates, coord target, int len) {
# define PW 5
  //We divide the coordinates array into n subarrays where n is the number of parallel processes
  //The results get written into a new array, where we directly calculate the minimal distance
  omp_set_num_threads(PW);
  coord candidates[PW]; //we might wanna pad these coords since we could have false sharing at the end of the parallel block
  #pragma omp parallel
  {
    int id , start, plen;
    id = omp_get_thread_num();
    plen = len / PW;
    start = id * plen;
    std::string msg = "hi from thread " + std::to_string(id) + ", I start at " + std::to_string(start) + " and end at "
+ std::to_string(start+plen) + "\n";
    //std::cout << msg;
    if (start + plen >= len)
      plen = len - start;
    candidates[id] = find_nearest_optimized(&coordinates[start], target, plen);
  }
  return find_nearest_optimized(candidates, target, PW);
}

struct timeit {
    std::chrono::high_resolution_clock::time_point start, end;
    void tick() {
      start = std::chrono::high_resolution_clock::now();
    }
    void tock() {
      end = std::chrono::high_resolution_clock::now();
    }
    int kerBANG() {
      return std::chrono::duration_cast<std::chrono::microseconds>(end - start).count();
    }
};

std::vector<double> read_file(std::string filename) {
  std::ifstream in(filename);
  std::vector<double> coordinates{std::istream_iterator<double>(in), std::istream_iterator<double>()};
  return coordinates;
}

int main() {

  std::vector<double> coord_vec = read_file("Random_coordinates.txt");

  int n_entries = coord_vec.size()/2 - 1;

  std::vector<coord> coordinates(n_entries);
  coord target;
  target.y = coord_vec.back();
  coord_vec.pop_back();
  target.x = coord_vec.back();
  timeit timer;

  for (int i = 0; i < n_entries; i++) {
    coordinates[i].x = coord_vec[2*i];
    coordinates[i].y = coord_vec[2*i + 1];
  }

  timer.tick();
  coord result = find_nearest_naive(&coordinates[0], target, n_entries);
  timer.tock();
  std::cout << "the result of the naive approach is: \n";
  result.print();
  std::cout << "and it took " << timer.kerBANG() << " microsecs\n";

  timer.tick();
  coord result_fast = find_naive_opt(&coordinates[0], target, n_entries);
  timer.tock();
  std::cout << "the result of the optimised naive approach is: \n";
  result_fast.print();
  std::cout << "and it took " << timer.kerBANG() << " microsecs\n";

  timer.tick();
  coord result_very_fast = find_nearest_optimized(&coordinates[0], target, n_entries);
  timer.tock();
  std::cout << "the result of the optimised approach is:\n";
  result_fast.print();
  std::cout << "and it took " << timer.kerBANG() << " microsecs\n";

  timer.tick();
  coord result_very_very_fast = find_nearest_optimized_v2(&coordinates[0], target, n_entries);
  timer.tock();
  std::cout << "the result of the optoptimised approach is:\n";
  result_very_very_fast.print();
  std::cout << "and it took " << timer.kerBANG() << " microsecs\n";

  timer.tick();
  coord result_parallel = find_nearest_parallel(&coordinates[0], target, n_entries);
  timer.tock();
  std::cout << "the result of the parallelised approach is:\n";
  result_parallel.print();
  std::cout << "and it took " << timer.kerBANG() << " microsecs\n";

  return 0;
}
