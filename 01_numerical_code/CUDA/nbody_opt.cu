/**********************************************************************/
// An unoptimized Naive N-Body solver for Gravity Simulations         //
// G is assumed to be 1.0                                             //
// Course Material for HPCSE-II, Spring 2019, ETH Zurich              //
// Authors: Sergio Martin                                             //
// License: Use if you like, but give us credit.                      //
/**********************************************************************/

//TODO: EXPERIMENT WITH METAPARAMETERS!

#include <stdio.h>
#include <math.h>
#include "string.h"
#include <chrono>
#include <iostream>

__constant__ double* dxPos;
__constant__ double* dyPos;
__constant__ double* dzPos;
__constant__ double* dmass;

//FAST INVERSE SQUARE ROOT FUNCTION WILL PROBS NEED BE COPIED INTO CODE SECTION AS INLINING MIGHT NOT ALWAYS BE OPTIMALLY SUPPORTED
//IN: DOUBLE NUMBER
//OUT: THIRD APPROXIMATION ITERATION OF ITS SQUARE ROOT
__device__ __forceinline__ double invsqrtQuake( double number ) //found on https://stackoverflow.com/questions/11644441/fast-inverse-square-root-on-x64
  {
      double y = number;
      double x2 = y * 0.5;
      std::int64_t i = *(std::int64_t *) &y;
      // The magic number for doubles is from https://cs.uwaterloo.ca/~m32rober/rsqrt.pdf
      i = 0x5fe6eb50c7b537a9 - (i >> 1);
      y = *(double *) &i;
      y = y * (1.5 - (x2 * y * y));   // 1st iteration
      y = y * (1.5 - (x2 * y * y));   // 2nd iteration, this can be removed
      y = y * (1.5 - (x2 * y * y));   // 3rd iteration for extra precision
      return y;
  }
	
void checkCUDAError(const char *msg);

__global__ void forceKernel(double* xPos, double* yPos, double* zPos, double* mass, double* xFor, double* yFor, double* zFor, const size_t N, const size_t _nblocks)
{
 const size_t _idx = threadIdx.x;
 const size_t _bid = blockIdx.x;
 const size_t _bthreads = blockDim.x;
 const size_t m = _bid*_bthreads+_idx;			//thread-associated body index
  
 double t_xforce = 0.0;
 double t_yforce = 0.0;
 double t_zforce = 0.0; 	//thread-associated reduction result

 extern __shared__ double stuff[];
 double * _xPos = &stuff[0];
 double * _yPos = &stuff[_bthreads];
 double * _zPos = &stuff[2*_bthreads];
 double * _mass = &stuff[3*_bthreads];

 
 const double l_xloc = xPos[m];
 const double l_yloc = yPos[m];
 const double l_zloc = zPos[m];
 const double l_mass = mass[m];	 //initialising persistent point information

 double d_x;
 double d_y;
 double d_z;
 double _fc;
 double _ri;

for (int j = 0; j< _nblocks; j++) {

	t_xforce = 0;
  	t_yforce = 0;
	t_zforce = 0;

	__syncthreads();

	_xPos[_idx] = xPos[j*_bthreads + _idx];
 	_yPos[_idx] = yPos[j*_bthreads + _idx];
	_zPos[_idx] = zPos[j*_bthreads + _idx];
	_mass[_idx] = mass[j*_bthreads + _idx]; 

	__syncthreads();
	

	if (j!= _bid) {

		for (size_t i = 0 ; i < _bthreads; i++) {

  			d_x = l_xloc - _xPos[i];
  			d_y = l_yloc - _yPos[i];
  			d_z = l_zloc - _zPos[i];

  			_ri =  d_x * d_x + d_y * d_y + d_z * d_z;

  			_fc = l_mass*mass[i]*invsqrtQuake(_ri*_ri*_ri);

  			t_xforce += d_x * _fc;
  			t_yforce += d_y * _fc;
  			t_zforce += d_z * _fc;

		}
	} else {

		for (size_t i = 0 ; i < _bthreads; i++) if (i!=_idx) {

  			d_x = l_xloc - _xPos[i];
  			d_y = l_yloc - _yPos[i];
  			d_z = l_zloc - _zPos[i];

  			_ri =  d_x * d_x + d_y * d_y + d_z * d_z;

  			_fc = l_mass*mass[i]*invsqrtQuake(_ri*_ri*_ri);

  			t_xforce += d_x * _fc;
  			t_yforce += d_y * _fc;
  			t_zforce += d_z * _fc;

		}
	}
	xFor[m] +=  t_xforce;
 	yFor[m] +=  t_yforce;
 	zFor[m] +=  t_zforce;
 }
}

int main(int argc, char* argv[])
{
 size_t N0 = 80;
 size_t N  = N0*N0*N0;

 // Initializing N-Body Problem

 double* xPos   = (double*) calloc (N, sizeof(double));
 double* yPos   = (double*) calloc (N, sizeof(double));
 double* zPos   = (double*) calloc (N, sizeof(double));
 double* xFor   = (double*) calloc (N, sizeof(double));
 double* yFor   = (double*) calloc (N, sizeof(double));
 double* zFor   = (double*) calloc (N, sizeof(double));
 double* mass   = (double*) calloc (N, sizeof(double));

 size_t current = 0;
 for (size_t i = 0; i < N0; i++)
 for (size_t j = 0; j < N0; j++)
 for (size_t k = 0; k < N0; k++)
 {
  xPos[current] = i;
  yPos[current] = j;
  zPos[current] = k;
  mass[current] = 1.0;
  xFor[current] = 0.0;
  yFor[current] = 0.0;
  zFor[current] = 0.0;
  current++;
 }

 // Calculating Kernel Geometry
 size_t threadsPerBlock  = 512;
 size_t blocksPerGrid    = ceil(double (((double)N) / ((double)threadsPerBlock)));

 // Allocating and initializing GPU memory

 		cudaMalloc((void **) &dxPos,  sizeof(double) * N); checkCUDAError("Unable to allocate storage on the device");
		cudaMalloc((void **) &dyPos,  sizeof(double) * N); checkCUDAError("Unable to allocate storage on the device");
 		cudaMalloc((void **) &dzPos,  sizeof(double) * N); checkCUDAError("Unable to allocate storage on the device");
 double* dxFor; cudaMalloc((void **) &dxFor,  sizeof(double) * N); checkCUDAError("Unable to allocate storage on the device");
 double* dyFor; cudaMalloc((void **) &dyFor,  sizeof(double) * N); checkCUDAError("Unable to allocate storage on the device");
 double* dzFor; cudaMalloc((void **) &dzFor,  sizeof(double) * N); checkCUDAError("Unable to allocate storage on the device");
 		cudaMalloc((void **) &dmass,  sizeof(double) * N); checkCUDAError("Unable to allocate storage on the device");
/* double* dxForTemp; cudaMalloc((void **) &dxForTemp,  sizeof(double) * N ); checkCUDAError("Unable to allocate storage on the device");
 double* dyForTemp; cudaMalloc((void **) &dyForTemp,  sizeof(double) * N ); checkCUDAError("Unable to allocate storage on the device");
 double* dzForTemp; cudaMalloc((void **) &dzForTemp,  sizeof(double) * N ); checkCUDAError("Unable to allocate storage on the device");*/

 cudaMemcpy(dxPos, xPos, sizeof(double) * N, cudaMemcpyHostToDevice); checkCUDAError("Failed Initial Conditions Memcpy");
 cudaMemcpy(dyPos, yPos, sizeof(double) * N, cudaMemcpyHostToDevice); checkCUDAError("Failed Initial Conditions Memcpy");
 cudaMemcpy(dzPos, zPos, sizeof(double) * N, cudaMemcpyHostToDevice); checkCUDAError("Failed Initial Conditions Memcpy");
 cudaMemcpy(dxFor, xFor, sizeof(double) * N, cudaMemcpyHostToDevice); checkCUDAError("Failed Initial Conditions Memcpy");
 cudaMemcpy(dyFor, yFor, sizeof(double) * N, cudaMemcpyHostToDevice); checkCUDAError("Failed Initial Conditions Memcpy");
 cudaMemcpy(dzFor, zFor, sizeof(double) * N, cudaMemcpyHostToDevice); checkCUDAError("Failed Initial Conditions Memcpy");
 cudaMemcpy(dmass, mass, sizeof(double) * N, cudaMemcpyHostToDevice); checkCUDAError("Failed Initial Conditions Memcpy");


 // Running Force-calculation kernel
 auto startTime = std::chrono::system_clock::now();
 forceKernel<<<blocksPerGrid, threadsPerBlock, size_t(4*threadsPerBlock*sizeof(double)) >>>(dxPos, dyPos, dzPos, dmass, dxFor, dyFor, dzFor, N, blocksPerGrid); checkCUDAError("Failed Force Kernel");
 cudaDeviceSynchronize();
 auto endTime = std::chrono::system_clock::now();

 cudaMemcpy(xFor, dxFor, sizeof(double) * N, cudaMemcpyDeviceToHost); checkCUDAError("Failed Final Conditions Memcpy");
 cudaMemcpy(yFor, dyFor, sizeof(double) * N, cudaMemcpyDeviceToHost); checkCUDAError("Failed Final Conditions Memcpy");
 cudaMemcpy(zFor, dzFor, sizeof(double) * N, cudaMemcpyDeviceToHost); checkCUDAError("Failed Final Conditions Memcpy");
 
 double netForce = 0.0;
 double absForce = 0.0;
 for (size_t i = 0; i < N; i++) netForce += xFor[i] + yFor[i] + zFor[i];
 for (size_t i = 0; i < N; i++) absForce += abs(xFor[i] + yFor[i] + zFor[i]);

 printf("     Net Force: %.8f\n", netForce);
 printf("Absolute Force: %.6f\n", absForce);
 printf("	   Time: %.8fs\n", std::chrono::duration<double>(endTime-startTime).count());

 if (isfinite(netForce) == false)      { printf("Verification Failed: Net force is not a finite value!\n"); exit(-1); }
 if (fabs(netForce) > 0.00001)         { printf("Verification Failed: Force equilibrium not conserved!\n"); exit(-1); }
 if (isfinite(absForce) == false)      { printf("Verification Failed: Absolute Force is not a finite value!\n"); exit(-1); }

  return 0;
}

void checkCUDAError(const char *msg)
{
 cudaError_t err = cudaGetLastError();
 if( cudaSuccess != err)
 {
  fprintf(stderr, "CUDA Error: %s: %s.\n", msg, cudaGetErrorString(err) );
  exit(EXIT_FAILURE);
 }
}
