function location = brownian_motion(T, N)
stepsize = sqrt(T/N);
location = zeros(1,N);
for i = 2 : N
    location(i) = location(i-1) + normrnd(0.0,stepsize);
end
end
