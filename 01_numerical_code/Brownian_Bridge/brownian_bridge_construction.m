function brownian_bridge_construction()
n = 11;
interval = 1.0/(n-1);
refinements = 8;
locations = zeros(refinements + 1, 10*2^refinements+1);
times = zeros(refinements + 1, 10*2^refinements+1);



locations(1,1:n) = brownian_motion(1.0, n);

for i = 2:n
    times(1,i) = times(1,i-1) + interval;
end

plot(times(1,1:n),locations(1,1:n)); hold on

for i = 2:(refinements + 1)
    for j = 1:(n-1)
        %copy the locations for plotting purposes
        locations(i,2*j-1) = locations(i-1,j);
        locations(i, 2*j) = brownian_bridge(locations(i-1, j), locations(i-1, j+1), interval);
        times(i,2*j) = times(i, 2*j-1) + interval/2.0;
        times(i,2*j + 1) = times(i, 2*j-1) + interval;
    end
    locations(i,2*n - 1) = locations(i-1,n);

    interval = interval/2.0;
    n = 2*n - 1;
    plot(times(i,1:n),locations(i,1:n));
end


xlabel('time');
ylabel('location'); hold off

end

function location = brownian_motion(T, N)
stepsize = sqrt(T/N);
location = zeros(1,N);
for i = 2 : N
    location(i) = location(i-1) + normrnd(0.0,stepsize);
end
end

function res = brownian_bridge(left, right, stepsize)
 res = normrnd((left + right)/2.0, sqrt(stepsize)/2.0);
end
