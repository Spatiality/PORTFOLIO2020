A Course project for advanced machine learning:
From incomplete pre-processed brain CT we reconstruct the age of the participants. 

Authors: Ming Yi & Florian Schweizer

Prerequisites: Python 3, scikit-learn, Keras, NumPy, tensorflow, pandas

usage: run with 'Python3 task1_age_Prediction_OG.py' respectively 'Python3 task1_age_Prediction.py'
