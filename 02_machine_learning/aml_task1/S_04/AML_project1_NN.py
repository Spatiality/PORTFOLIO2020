
import numpy as np
import pandas as pd


from keras.utils import np_utils
from keras.models import Sequential
from keras.layers import Dense, Dropout, BatchNormalization
from sklearn.preprocessing import StandardScaler
from keras.callbacks import ModelCheckpoint
from keras import backend as K

def r2_loss(y_true, y_pred):
    SS_res =  K.sum(K.square(y_true - y_pred)) 
    SS_tot = K.sum(K.square(y_true - K.mean(y_true))) 
    return -( 1 - SS_res/(SS_tot + K.epsilon()) )




def Model(N, ActFun):
    # Create model
    ann = Sequential()
    neurons = 400
    ann.add(Dense(neurons, input_dim=N, activation=ActFun))  # leaky activation
    ann.add(Dropout(0.4))
    ann.add(BatchNormalization())
    ann.add(Dense(neurons, activation=ActFun))
    ann.add(Dropout(0.4))
    ann.add(BatchNormalization())
    ann.add(Dense(neurons, activation=ActFun))
    ann.add(Dropout(0.4))
    ann.add(BatchNormalization())
    ann.add(Dense(neurons, activation=ActFun))
    ann.add(Dropout(0.4))
    ann.add(BatchNormalization())
    ann.add(Dense(neurons, activation=ActFun))
    ann.add(Dense(1))
    # Compile model
    ann.compile(loss=r2_loss, optimizer='nadam', metrics=[r2_loss])
    return ann


def Supervised_learning(file_train_label, file_test, file_w):
    with  open(file_w, 'w') as fw:
        # read data from csv file
        train_labeled = pd.read_csv(file_train_label)  # 9000 * 140
        test = pd.read_csv(file_test)
        id = np.array(test.index.values).reshape(-1, 1)

        train_labeled = train_labeled.sample(frac=1).reset_index(drop=True)
        test_x = np.array(test.iloc[:,1:])

        X_labeled = np.array(train_labeled.drop(columns= ['y','id'] ))
        y_labeled = np.array(train_labeled['y']).reshape(-1, 1)

        # train labelled
        ann1 = Model(X_labeled[0].size, 'sigmoid')
        print(X_labeled.shape)


        # Add Checkpoints to save best result re. X-validation
        filepath = 'weights.best.csv'
        checkpoint = ModelCheckpoint(filepath, monitor = 'val_loss', save_best_only = True, mode = 'min')
        
        ann1.fit(X_labeled, y_labeled, epochs=150, batch_size=64, callbacks = [checkpoint], validation_split=0.05)
        ann1.load_weights(filepath)

        y_predict_test = ann1.predict(test_x)

        # write the predicted labels into CSV file
        id_label = np.concatenate((id, y_predict_test.reshape(-1, 1)), axis=1)
        id_label_df = pd.DataFrame(id_label, columns=['id', 'y'])
        id_label_df.to_csv(fw, index=False)


if __name__ != 'main':
    Supervised_learning("train_relevant.csv", "test_relevant.csv", "result.csv")

