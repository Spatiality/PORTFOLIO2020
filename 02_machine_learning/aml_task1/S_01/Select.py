import pandas as pd
import sklearn as skl
import numpy as np

import matplotlib
import matplotlib.pyplot as plt
from sklearn.pipeline import Pipeline, FeatureUnion
from sklearn.model_selection import GridSearchCV
from sklearn.svm import SVR
from sklearn.decomposition import PCA
from sklearn.feature_selection import SelectKBest, GenericUnivariateSelect, chi2, mutual_info_regression, RFECV
from sklearn.linear_model import LassoCV, ElasticNetCV
from sklearn import preprocessing
import seaborn as sns




traindata = pd.read_csv("Train_repaird.csv")
testdata = pd.read_csv("X_test_repaird.csv")

X = traindata.iloc[:,1:-2]  #independent columns
y = traindata.iloc[:,-1]    #target column i.e age

x = X.values #returns a numpy array
min_max_scaler = preprocessing.MinMaxScaler()
x_scaled = min_max_scaler.fit_transform(x)
X = pd.DataFrame(x_scaled)

pca = PCA(n_components=831)

#X = pca.fit_transform(X)

traindata.iloc[:,1:-2] = X


# Elastic REGRESSION
reg = ElasticNetCV(cv = 15, n_jobs = -1,l1_ratio = 0.7 ,alphas = list(np.arange(0.001,0.02,0.0001)), selection = 'random', max_iter = 10000000,precompute = True)
reg.fit(X, y)
print("Best alpha using built-in ElNetCV: %f" % reg.alpha_)
print("Best L1 - L2 -ratio using ElNetCV: %f" % reg.l1_ratio_)
print("Best score using built-in ElNetCV: %f" %reg.score(X,y))
coef = pd.Series(reg.coef_, index = traindata.iloc[:,1:-2].columns)

print("ElasticNet picked " + str(sum(coef != 0)) + " variables and eliminated the other " +  str(sum(coef == 0)) + " variables")

imp_coef = coef.sort_values()

matplotlib.rcParams['figure.figsize'] = (8.0, 10.0)
imp_coef.plot(kind = "barh")
plt.title("Feature importance using ElNEt Model")
#plt.show()

#NOW TO CLEAN THE DATASET

abscoef = coef.abs()
while sum(abscoef==0) > 0 :
    idx = abscoef.idxmin()
    traindata = traindata.drop(idx, axis = 1)
    testdata  = testdata.drop( idx, axis = 1)
    print(str(idx) + " - Variable dropped, had importance of ", abscoef[idx])
    abscoef = abscoef.drop(idx)

with open("train_relevant.csv", 'w') as fw:
    traindata.to_csv(fw, index=False)

with open("test_relevant.csv", 'w') as fw:
    testdata.to_csv(fw, index=False)



"""
#RECURSIVE FEATURE ELIMINATION
estimator = SVR(kernel="linear")
selector = RFECV(estimator, n_jobs = -1, cv = 5,  step=3, verbose = 10)
selector = selector.fit(X, y)
print('support of selector: ',selector.support_)


print('ranking of selector: ',selector.ranking_)
"""

"""
#JOINT FEATURE ELIMINATION & PCA 
# This dataset is way too high-dimensional. Better do PCA:
pca = PCA(n_components=100)

# Maybe some original features where good, too?
selection = SelectKBest(k=300)
#selection = GenericUnivariateSelect(param=20, chi2, 'k_best')
# Build estimator from PCA and Univariate selection:

combined_features = FeatureUnion([("pca", pca), ("univ_select", selection)])

# Use combined features to transform dataset:
X_features = combined_features.fit(X, y).transform(X)
print("Combined space has", X_features.shape[1], "features")

svm = SVR(kernel="linear", gamma = 'scale')

# Do grid search over k, n_components and C:

pipeline = Pipeline([("features", combined_features), ("svm", svm)])

param_grid = dict(features__pca__n_components=list(range(32,37)),
                  features__univ_select__k=[200,300,400],
                  svm__C=[1.0])

grid_search = GridSearchCV(pipeline, param_grid=param_grid, cv=10, verbose=10)
grid_search.fit(X, y)
print(grid_search.best_estimator_)
"""
"""
# LASSO REGRESSION
reg = LassoCV(cv = 10, n_jobs = -1, selection = 'random', max_iter = 10000000,precompute = True)
reg.fit(X, y)
print("Best alpha using built-in LassoCV: %f" % reg.alpha_)
print("Best score using built-in LassoCV: %f" %reg.score(X,y))
coef = pd.Series(reg.coef_, index = traindata.iloc[:,1:-2].columns)
print (coef.head())

print("Lasso picked " + str(sum(coef != 0)) + " variables and eliminated the other " +  str(sum(coef == 0)) + " variables")

imp_coef = coef.sort_values()
matplotlib.rcParams['figure.figsize'] = (8.0, 10.0)
imp_coef.plot(kind = "barh")
plt.title("Feature importance using Lasso Model")
"""
