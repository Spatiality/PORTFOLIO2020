import pandas as pd
import sklearn as sk
import numpy as np

xdata = pd.read_csv("X_train.csv")
ydata = pd.read_csv("y_train.csv")

print(xdata.head())
print(ydata.head())

xdata['y'] = ydata['y']

print(xdata.head())

train_sorted = xdata.sort_values('y')
print(train_sorted.head())

train_np = train_sorted.iloc[:,1:].to_numpy()

#Now to repair the training set: by finding the local median!

#first go through all entries
count = 0
size = train_np.size

for i in train_np:
#then go through all variables
    varcount = 0
    for x in i[:-2] :
        if np.isnan(x):
            regAr = np. empty( [2,1] )
            X_ = np.empty([1])
            start = max(0,count - 5)
            end = min(size, count + 5)
            itcount = 0
            for it in train_np[start : end]:
                if not np.isnan(it[varcount]):
                    if not itcount : 
                        X_[0] = it[varcount]
                        #regAr[0] = [it[-1], 1.0]
                    else :
                        X_ = np.append(X_, [it[varcount]])
                        #regAr = np.stack(regAr,[it[-1], 0])
                    x_sorted = np.sort (X_)
                    i[varcount] = x_sorted[int(X_.size/2)]
                itcount += 1 
        varcount += 1
    count += 1  
"""
varcount = 0
for x in train_np[0] :
    if np.isnan(x):
        regAr = np. empty( [2,1] )
        X_ = np.empty([1])
        start = 0
        end = min(size, 0 + 7)
        itcount = 0
        for it in train_np[start : end]:
            if not np.isnan(it[varcount]):
                if not itcount : 
                    X_[0] = it[varcount]
                    regAr[0] = [it[-1], 1.0]
                else :
                    X_ = np.append(X_, [it[varcount]])
                    #regAr = np.stack(regAr,[it[-1], 0])
                x_sorted = np.sort (X_)
                i[varcount] = x_sorted[int(X_.size/2)]
            itcount += 1
    varcount += 1

varcount = 0
for x in train_np[-1] :
    if np.isnan(x):
        regAr = np. empty( [2,1] )
        X_ = np.empty([1])
        start = max(0,size - 5)
        end = size
        itcount = 0
        for it in train_np[start : end]:
            if not np.isnan(it[varcount]):
                if not itcount : 
                    X_[0] = it[varcount]
                    regAr[0] = [it[-1], 1.0]
                else :
                    X_ = np.append(X_, [it[varcount]])
                    #regAr = np.stack(regAr,[it[-1], 0])
                x_sorted = np.sort (X_)
                i[varcount] = x_sorted[int(X_.length()/2)]
            itcount += 1
    varcount += 1
"""
train_sorted.iloc[:,1:] = train_np

print(train_sorted.head())

with open("Train_repaird.csv", 'w') as fw:
    train_sorted.to_csv(fw, index=False)

# Now only thing left is imputing test data for X_test

xTdata = pd.read_csv("X_test.csv")
np_xTest = xTdata.iloc[:,1:].to_numpy()

print('Test Data to be repaired:')
print(xTdata.head())


from sklearn.experimental import enable_iterative_imputer
from sklearn.impute import IterativeImputer
imp = IterativeImputer(max_iter=20, random_state=42)
imp.fit(train_np[:,:-1])

xTdata.iloc[:,1:] = imp.transform(np_xTest)


print('Repaired Test Data:')
print(xTdata.head())

with open("X_test_repaird.csv", 'w') as fw:
    xTdata.to_csv(fw, index=False)



