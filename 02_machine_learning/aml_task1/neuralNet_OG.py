import numpy as np
import pandas as pd
from keras.models import Sequential
from keras.callbacks import ModelCheckpoint
from keras.layers import Dense, Dropout, BatchNormalization, Lambda, Layer, GaussianNoise
from sklearn.preprocessing import StandardScaler
import tensorflow as tf
from keras import backend as K
from sklearn.model_selection import StratifiedKFold, train_test_split


# custom matrix function
def coeff_determination(y_true, y_pred):

    SS_res =  K.sum(K.square( y_true-y_pred ))
    SS_tot = K.sum(K.square( y_true - K.mean(y_true)))
    return -SS_res/(SS_tot + K.epsilon()) + 1.0


class RBFLayer(Layer):
    def __init__(self, units, gamma, **kwargs):
        super(RBFLayer, self).__init__(**kwargs)
        self.units = units
        self.gamma = K.cast_to_floatx(gamma)

    def build(self, input_shape):
        self.mu = self.add_weight(name='mu',
                                  shape=(int(input_shape[1]), self.units),
                                  initializer='uniform',
                                  trainable=True)
        super(RBFLayer, self).build(input_shape)

    def call(self, inputs):
        diff = K.expand_dims(inputs) - self.mu
        l2 = K.sum(K.pow(diff,2), axis=1)
        res = K.exp(-1 * self.gamma * l2)
        return res

    def compute_output_shape(self, input_shape):
        return (input_shape[0], self.units)


def Model(feature_num ,neurons):
    # Create model
    act = 'relu'
    d_loss = 0.37
    ann = Sequential()
    ann.add(GaussianNoise(.0015,input_shape = [feature_num])) #Gaussian Noise layer to reduce overfitting 
    ann.add(Dense(neurons, activation=act))  # leaky
    ann.add(Dropout(d_loss))
    ann.add(BatchNormalization())
    ann.add(Dense(neurons, activation=act))
    ann.add(Dropout(d_loss))
    ann.add(BatchNormalization())
    ann.add(Dense(int(neurons), activation=act))
    ann.add(Dropout(d_loss))
    ann.add(BatchNormalization())
    ann.add(Dense(1))
    # Compile model
    ann.compile(loss='mse', optimizer='adam', metrics=[coeff_determination])
    return ann

def BigModel(feature_num, neurons = 450):
    # Create model
    act = 'sigmoid'
    d_loss = 0.4
    ann = Sequential()
    ann.add(GaussianNoise(.002,input_shape = [feature_num])) #Gaussian Noise layer to reduce overfitting 
    ann.add(Dense(neurons, activation=act))  # leaky
    ann.add(Dropout(d_loss))
    ann.add(BatchNormalization())
    ann.add(Dense(neurons, activation=act))
    ann.add(Dropout(d_loss))
    ann.add(BatchNormalization())
    ann.add(Dense(int(neurons), activation=act))
    ann.add(Dropout(d_loss))
    ann.add(BatchNormalization())
    ann.add(Dense(1))
    # Compile model
    ann.compile(loss='mse', optimizer='adam', metrics=[coeff_determination])
    return ann


def batchsize(i) :
    return 32 + 48*i

def epochs(j) :
    return j*30

def good_split_learning(train_x, train_y, ann1, filepath) :
    epochs = 950
    naive = ann1.get_weights()
    age_group = []
    for i in train_y:
        age_group.append([i - i%5])

    X_train, X_test, y_train, y_test = train_test_split(train_x, train_y, test_size=0.18, stratify = age_group)
    oldscore = -2.0
    bestepoch = 0
    for i in range(0,epochs) :
        ann1.fit(X_train, y_train, verbose = 0, epochs=1, batch_size=128)
        score = ann1.evaluate(X_test, y_test, verbose = 0)[1]
        if score > oldscore :
            oldscore = score
            ann1.save_weights(filepath)
            print('new best score: ', score)
            bestepoch = i
    if oldscore < 0.665 :
        ann1.set_weights(naive)
        good_split_learning(train_x, train_y, ann1, filepath)
    else:
        print('done with learning, got high score of ' , oldscore, ' at epoch ', bestepoch)
        ann1.load_weights(filepath)
        for i in range(bestepoch,epochs) :
            ann1.fit(X_test, y_test, verbose = 0, epochs=1, batch_size=128)
            score = ann1.evaluate(X_train, y_train, verbose = 0)[1]
            if score > 0.75 :
                oldscore = score
                ann1.save_weights(filepath)
                print('new best score: ', score)
                bestepoch = i    

    


def CV_learning(train_x, train_y, ann, filepath) :
    kfold = StratifiedKFold(n_splits=10, shuffle=True)
    cvscores = np.zeros(24)
    age_group = []
    for i in train_y:
        age_group.append([i - i%10])
    naive = ann.get_weights()
    for train, test in kfold.split(train_x, age_group) :
        for i in range(0,4) :     #batch size range range
            ann.set_weights(naive)
            ann.fit(train_x[train], train_y[train], epochs = 80, batch_size = batchsize(i))
            for j in range(0,6) : #epoch range  
                ann.fit(train_x[train], train_y[train], epochs = epochs(1),batch_size =  batchsize(i))
                cvscores[i*4 + j] += ann.evaluate(train_x[test], train_y[test])[1]/10.0

    idx = np.where(cvscores == np.amax(cvscores))
    print('CV Scores:')
    print(cvscores)
    scores = pd.DataFrame(cvscores.reshape(4,-1), columns=['0','1','2','3'])
    scores.to_csv('cv_scores_NN')
    i_max = int(idx[0] /4)
    j_max = int(idx[0])% 6
    print('best batch size: ', batchsize(i_max), 'best j: ', j_max)
    ann.set_weights(naive)
    ann.fit(train_x, train_y, epochs = 80 + epochs(j_max), batch_size = 32 + 48*i_max)
    ann.save_weights(filepath)

def simplePredictAge(train_x, train_y, test_x, test_y, neurons = 450) :
    # Feature Scaling, 0 mean and 1 standard variance
    scalar = StandardScaler()
    train_x = scalar.fit_transform(train_x.astype('float64'))
    test_x = scalar.transform(test_x.astype('float64'))

    # train labeled
    ann1 = BigModel(int(train_x.shape[1]), neurons)
    epochs = 1500
    naive = ann1.get_weights()
    age_group = []
    for i in train_y:
        age_group.append([i - i%10])

    X_train, X_test, y_train, y_test = train_test_split(train_x, train_y, test_size=0.08, stratify = age_group)
    oldscore = -2.0
    bestepoch = 0
    UPDATE = 0
    for i in range(0,epochs) :
        ann1.fit(X_train, y_train, verbose = 0, epochs=1, batch_size=128)
        score = ann1.evaluate(X_test, y_test)[1]
        UPDATE += 1
        if score > oldscore :
            oldscore = score
            print('new best score: ', score)
            bestepoch = i
            UPDATE = 0
        if UPDATE > 300 :
            break
    ann1.set_weights(naive)
    ann1.fit(train_x, train_y, verbose = 0, epochs = bestepoch, batch_size = 128)
    
    score = ann1.evaluate(test_x, test_y)[1]
    print('model evaluated, had a score of ' , score)
    return np.max([-0.5,score]) 

        


def predictAge(train_x, train_y, test_x, neurons = 180, remember = False):
    # Feature Scaling, 0 mean and 1 standard variance
    scalar = StandardScaler()
    train_x = scalar.fit_transform(train_x.astype('float64'))
    test_x = scalar.transform(test_x.astype('float64'))

    # train labeled
    ann1 = Model(int(train_x.shape[1]), neurons)
    print(train_x.shape)

    filepath = 'weights.best.csv'
    checkpoint = ModelCheckpoint(filepath, monitor = 'val_coeff_determination', verbose = 1, save_best_only = True, mode = 'max')
    if remember == False :
        #CV_learning(train_x, train_y, ann1, filepath)
#        ann1.fit(train_x, train_y, epochs=500, batch_size=128, callbacks = [checkpoint], validation_split=0.06)
#        ann1.fit(train_x, train_y, epochs=230, batch_size=128)
#        ann1.save_weights(filepath)
        good_split_learning(train_x,train_y, ann1, filepath)

    ann1.load_weights(filepath)

    # Predict unlabeled
    predict_y = ann1.predict(test_x) 

    return predict_y
