import pandas
import numpy as np
from sklearn.impute import SimpleImputer
from sklearn.ensemble import IsolationForest
from sklearn.feature_selection import SelectKBest, f_regression
from neuralNet import predictAge
from sklearn.decomposition import PCA
from sklearn.linear_model import ElasticNetCV


# filling missing values
# idea: group the data by a range 10 and find the median of each feature in that group
def fill_missing_values(X_train_file, y_train_file, X_predict_file = None):
    # read from files
    x_train = pandas.read_csv(X_train_file) # 1212*833
    y_train = pandas.read_csv(y_train_file,index_col='id') # 1212*1
    if X_predict_file:
        x_predict = pandas.read_csv(X_predict_file)
        sample_file = pandas.read_csv('sample.csv')
        sample_file['id'] = x_predict['id']
        sample_file.to_csv('sample.csv', index=False)


    # concatenate dataframes together along column
    xy_train = pandas.concat([x_train, y_train],axis = 1)

    # get the smallest and the largest age
    all_ages = y_train.get('y') # a list
    min_age = min(all_ages)
    max_age = max(all_ages)
    age_range = 12
    if X_predict_file:
        age_range = 5

    # group data by ages range
    xy_grouped = xy_train.groupby('y')
    xy_ranged_dict = {}
    for name, group in xy_grouped: # i.e. keys and values in dictionary
        age_key = name - name%age_range
        groups = xy_ranged_dict.get(age_key,[])
        groups.append(group)
        xy_ranged_dict[age_key] = groups
    # impute in every group; concatenate together
    count = 0
    for age in xy_ranged_dict:
        group_init = xy_ranged_dict[age][0]
        length = len(xy_ranged_dict[age])
        for g in range(1,length):
            group_init = pandas.concat([group_init,xy_ranged_dict[age][g]], axis = 0)
        # fill in missing values
        imp = SimpleImputer(missing_values= np.nan, strategy='median')
        imp.fit(group_init)
        xy_ranged_dict[age] = imp.transform(group_init)
        # put filled features together
        if count == 0:
            xy_filled = xy_ranged_dict[age]
        else:
            #print('were in age group ', age - age%age_range , ' - ' , age -age % age_range + age_range, ' now.')
            xy_filled = np.concatenate((xy_filled,xy_ranged_dict[age]), axis = 0)
        count += 1
    # xy_filled = xy_filled.squeeze()
    # use the train dataset to fit test dataset
    imp.fit(xy_filled[:, :-1])
    if X_predict_file:
        x_predict = imp.transform(x_predict)
        # random shutter
        np.random.shuffle(xy_filled)

    if X_predict_file:
        return xy_filled, x_predict   # 1212*834
    else :
        return xy_filled

def remove_anomalies(xy_dataset):
    handler = IsolationForest(n_estimators = 11, contamination= 0.11, behaviour="new", bootstrap = True, random_state = 42)
    handler.fit(xy_dataset)
    label = handler.fit_predict(xy_dataset)
    # score = handler.decision_function(X_dataset)
    count = 0
    # remove anomolies, delete from back of the list
    for i in range(xy_dataset.shape[0] - 1, -1, -1):
        if label[i] == -1:
            count += 1
            xy_dataset = np.delete(xy_dataset, i, 0)
    print("anomalies: ", count)
    print(xy_dataset.shape)
    # split X and y
    X_train = xy_dataset[:, :-1]
    y_train = xy_dataset[:, -1]
    return X_train, y_train

def feature_selection(x_train, y_train, x_predict):
    # apply SelectKBest class to extract top 10 best features
    selector = SelectKBest(score_func = f_regression, k=120)
    x_train_selected = selector.fit_transform(x_train, y_train)
    x_predict_selected = selector.transform(x_predict)
    print(x_train_selected.shape)

#     dfscores = pandas.DataFrame(selector.scores_)
#     dfcolumns = pandas.DataFrame(np.arange(833))
#     # concat two dataframes for better visualization
#     featureScores = pandas.concat([dfcolumns, dfscores], axis=1)
#     featureScores.columns = ['Specs', 'Score']  # naming the dataframe columns
#     print(featureScores.nlargest(60, 'Score'))  # print 10 best features
    return x_train_selected, x_predict_selected, selector

def result_to_csv(predict_y, sample_file):
    # write the result to the CSV file
    sample_file = pandas.read_csv(sample_file)
    id = sample_file['id'].to_numpy().reshape(-1,1)
    result = np.concatenate((id, np.round(predict_y).reshape(-1, 1)), axis=1)
    result = pandas.DataFrame(result, columns=['id', 'y'])
    result.to_csv('predict_y.csv', index=False)

def repair_by_NN(x_test_raw, y_predicted, selector):
    filled_files = fill_missing_values(x_test_raw, y_predicted)
    sample_file = pandas.read_csv('sample.csv')
    sample_file['id'] = pandas.DataFrame(filled_files[:,0].reshape(-1,1), columns = ['id'])['id']
    sample_file.to_csv('sample.csv', index=False)
    return selector.transform(filled_files[:, :-1])

if __name__ == '__main__':
    # testing
    # median_test = fill_missing_values('median_testing.csv','median_y.csv')
    # print(median_test)

    # preprocessing
    filled_files = fill_missing_values('X_train.csv', 'y_train.csv', 'X_test.csv')
    xy_train = filled_files[0]
    x_predict = filled_files[1]
    anomalFree = remove_anomalies(xy_train)
    X_train = anomalFree[0]
    y_train = anomalFree[1]

    selected = feature_selection(X_train, y_train, x_predict)
#    selected = feature_selection_by_elastic(X_train, y_train, x_predict)
    x_train_selected = selected[0]
    x_predict_selected = selected[1]

    # train and write to result to file
    y_predict = predictAge(x_train_selected, y_train, x_predict_selected)
    result_to_csv(y_predict, 'sample.csv')
    # repair the test file with a better hunch of the target values
    x_predict_selected = repair_by_NN('X_test.csv','predict_y.csv', selected[2])
    y_predict = predictAge(x_train_selected, y_train, x_predict_selected, True)
    
    result_to_csv(y_predict, 'sample.csv')
