import numpy as np
import pandas as pd
from keras.models import Sequential
from keras.callbacks import ModelCheckpoint
from keras.layers import Dense, Dropout, BatchNormalization, Lambda
from sklearn.preprocessing import StandardScaler
import tensorflow as tf


# custom matrix function
def coeff_determination(y_true, y_pred):
    from keras import backend as K
    SS_res =  K.sum(K.square( y_true-y_pred ))
    SS_tot = K.sum(K.square( y_true - K.mean(y_true)))
    return -SS_res/(SS_tot + K.epsilon()) + 1.0


def Model(feature_num):
    # Create model
    act = 'sigmoid'
    ann = Sequential()
    neurons = 350
    ann.add(Dense(neurons, input_dim=feature_num, activation=act))  # leaky
    ann.add(Dropout(0.5))
    ann.add(BatchNormalization())
    ann.add(Dense(neurons, activation=act))
    ann.add(Dropout(0.5))
    ann.add(BatchNormalization())
    ann.add(Lambda(lambda v: tf.cast(tf.spectral.fft(tf.cast(v,dtype=tf.complex64)),tf.float32)))
    ann.add(Dense(neurons, activation=act))
    ann.add(Dropout(0.5))
    ann.add(BatchNormalization())
    ann.add(Dense(1))
    # Compile model
    ann.compile(loss='mse', optimizer='nadam', metrics=[coeff_determination])
    return ann


def predictAge(train_x, train_y, test_x, remember = False):
    # Feature Scaling, 0 mean and 1 standard variance
    scalar = StandardScaler()
    train_x = scalar.fit_transform(train_x.astype('float64'))
    test_x = scalar.transform(test_x.astype('float64'))

    # train labeled
    ann1 = Model(train_x.shape[1])
    print(train_x.shape)

    filepath = 'weights.best.csv'
    checkpoint = ModelCheckpoint(filepath, monitor = 'val_coeff_determination', verbose = 1, save_best_only = True, mode = 'max')
    if remember == False :
        ann1.fit(train_x, train_y, epochs=1800, batch_size=130, callbacks = [checkpoint], validation_split=0.03)

    ann1.load_weights(filepath)

    # Predict unlabeled
    predict_y = ann1.predict(test_x) 

    return predict_y
