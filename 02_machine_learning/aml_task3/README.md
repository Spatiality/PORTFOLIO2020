A course project for advanced machine learning:
From noisy heartbeat recordings, we classify the the heart condition after standardising the signals & running some additional analytics on it.

Authors: Ming Yi & Florian Schweizer

Prerequisites: Python 3, scikit-learn, Keras, NumPy, tensorflow, pandas, bioSPPy, seaborn, matplotlib

usage: run with 'Python3 task1_classifier.py' respectively 'Python3 task1_svm.py'
